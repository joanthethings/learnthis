import { InfoLessonPublic, VideoLessonPublic } from './lesson.interface';

export interface PublicCourse {
	_id: string;
	url: string;
	title: string;
	background: string;
	description: string;
	image: string;
	price: number;
	compareAtPrice?: number;
}

export interface PublicSection {
	title: string;
	description?: string;
	visibility: boolean;
	lessons: Array<VideoLessonPublic | InfoLessonPublic>;
}

export interface PublicFullCourse extends PublicCourse {
	studentsCount?: number;
	sections: PublicSection[];
}
