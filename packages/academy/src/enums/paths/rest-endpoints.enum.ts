export enum RestEndPoints {
	LOGIN = '/api/login',
	LOGOUT = '/api/logout',
	PROFILE = '/api/profile',
	GITHUB_SIGNIN = '/api/github/signin',
	GITLAB_SIGNIN = '/api/gitlab/signin',
	GOOGLE_SIGNIN = '/api/google/signin',
	GITHUB_LINK = '/api/github/link',
	GITLAB_LINK = '/api/gitlab/link',
	GOOGLE_LINK = '/api/google/link',
	GITHUB_REDIRECT = '/api/github/redirect',
	GITLAB_REDIRECT = '/api/gitlab/redirect',
	GOOGLE_REDIRECT = '/api/google/redirect',
}
