import { createContext } from 'react';
import { StudentProfileOtherInfo } from '@Interfaces/student/student.interface';
import { ApolloQueryRefetch } from '@Interfaces/apollo/apollo-refetch.types';

interface IProfileContext {
	profile: StudentProfileOtherInfo;
	refetch: ApolloQueryRefetch;
}

export const ProfileContext = createContext<IProfileContext>({} as any);
