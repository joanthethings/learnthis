import ImgFallback from '@Components/generic/img-fallback';
import { FC } from 'react';

type ProfileHeaderProps = {
	photo?: string;
	name: string;
	surname: string;
	email: string;
	isMenu?: boolean;
};

export const ProfileHeader: FC<ProfileHeaderProps> = ({
	photo,
	name,
	surname,
	email,
	isMenu,
}) => (
	<div
		className={`p-1_5 flexcol-c-c ${
			isMenu ? '' : 'shadow-lg'
		} bg-white dark:bg-white-dark text-center text-white-dark dark:text-white`}>
		<ImgFallback
			className='rounded-full h-6 w-6 mb-1'
			src={photo}
			alt='Foto de perfil'
			fallbackSrc={`https://avatars.dicebear.com/api/initials/${name}-${surname}.svg`}
		/>

		<span className='text-lg font-semibold'>{`${name} ${surname}`}</span>
		<span className='break-all'>{email}</span>
	</div>
);

export default ProfileHeader;
