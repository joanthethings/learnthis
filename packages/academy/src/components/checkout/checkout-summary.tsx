import { ApolloError, QueryResult, useMutation } from '@apollo/client';
import Button from '@Components/generic/form/button/button';
import Loader from '@Components/loader';
import { AlertMessages } from '@Enums/config/constants';
import { MainPaths } from '@Enums/paths/main-paths.enum';
import { ApolloMutation } from '@Interfaces/apollo/apollo-query.types';
import { PublicCourse } from '@Interfaces/course/course.interface';
import { AuthState } from '@Interfaces/states/browser-context.interface';
import { AuthContext } from '@Lib/context/auth.context';
import { GraphqlOrder } from 'learnthis-utils';
import { NextRouter, useRouter } from 'next/router';
import { Dispatch, FC, ReactElement, SetStateAction, useContext } from 'react';
import { toast } from 'react-toastify';

export type CheckoutSummaryProps = {
	coursesInfo: QueryResult;
	clearShoppingCart: () => Promise<void>;
};

const CheckoutSummary: FC<CheckoutSummaryProps> = ({
	coursesInfo,
	clearShoppingCart,
}) => {
	const { data, loading } = coursesInfo;

	const { setAuth } = useContext(AuthContext);
	const router = useRouter();

	const { checkoutMutation } = getCheckoutMutation(
		setAuth,
		clearShoppingCart,
		router
	);

	let checkoutPrice;
	if (loading) {
		checkoutPrice = (
			<div className='flex-c-c p-1'>
				<Loader />
			</div>
		);
	} else {
		checkoutPrice = (
			<PricesComponent
				courses={data.course_public_find_by_url_array as PublicCourse[]}
			/>
		);
	}

	return (
		<div className='flexcol-c-c  px-1_25 py-1_5 bg-white dark:bg-white-dark text-white-dark dark:text-white rounded-lg shadow-lg'>
			<h2 className='text-center text-xl font-semibold mb-0_75'>RESUMEN</h2>
			{checkoutPrice}
			<div className='pt-0_5 w-full'>
				<Button
					kind='cta'
					className='w-full cursor-pointer'
					disabled={!(data && data.course_public_find_by_url_array.length > 0)}
					onClick={() =>
						onSubmitCheckout(
							checkoutMutation,
							data.course_public_find_by_url_array as PublicCourse[]
						)
					}>
					Comprar
				</Button>
			</div>
		</div>
	);
};

const getCheckoutMutation = (
	setAuth: Dispatch<SetStateAction<AuthState>>,
	clearShoppingCart: () => Promise<void>,
	router: NextRouter
) => {
	const mutation = GraphqlOrder.order_create;

	const [checkoutMutation, { loading }] = useMutation(mutation, {
		onCompleted: async (data: any) => {
			await clearShoppingCart();

			const coursesIds: string[] = data.order_create;

			setAuth(
				(oldState: AuthState): AuthState =>
					({
						...oldState,
						student: {
							...oldState.student,
							cart: [],
							coursesEnrolled: oldState.student?.coursesEnrolled.concat(
								coursesIds.map(course => ({ course }))
							),
						},
					} as AuthState)
			);

			router.push(MainPaths.INDEX);
			toast.success(AlertMessages.CHECKOUT_SUCCESS);
		},
		onError: (error: ApolloError) => {
			toast.error(error.message || AlertMessages.SERVER_ERROR);
		},
	});

	return { checkoutMutation, loading };
};

const onSubmitCheckout = async (
	checkoutMutation: ApolloMutation,
	courses: PublicCourse[]
) => {
	const coursesIds = courses.map(course => course._id);
	await checkoutMutation({
		variables: { coursesIds },
	});
};

export type PricesComponentProps = {
	courses: PublicCourse[];
};

const PricesComponent: FC<PricesComponentProps> = ({ courses }) => {
	let pricesComponent: ReactElement[] = [];

	if (courses.length > 0) {
		pricesComponent = courses.map((course: PublicCourse) => (
			<div
				key={course._id}
				className='flex-sb-c flex-wrap w-full font-semibold'>
				<p>
					{course.title.length > 25
						? course.title.substring(0, 25) + '...'
						: course.title}
				</p>
				<p>{course.price}€</p>
			</div>
		));
	}

	const totalPrice = Object.values(courses).reduce(
		(previousValue, { price }) => price + previousValue,
		0
	);

	return (
		<>
			{pricesComponent}
			<div className='flex-sb-c flex-wrap w-full text-xl font-semibold py-1 text-primary'>
				<p>TOTAL</p>
				<p>{totalPrice}€</p>
			</div>
		</>
	);
};

export default CheckoutSummary;
