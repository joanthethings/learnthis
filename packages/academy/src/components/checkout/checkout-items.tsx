import { QueryResult } from '@apollo/client';
import ImgFallback from '@Components/generic/img-fallback';
import { TrashIcon } from '@Components/icons/menu/trash-icon';
import Loader from '@Components/loader';
import { PublicCourse } from '@Interfaces/course/course.interface';
import { FC } from 'react';

export type CheckoutItemsProps = {
	coursesInfo: QueryResult;
	removeItemShoppingCart: (courseId: string) => Promise<void>;
};

const CheckoutItems: FC<CheckoutItemsProps> = ({
	coursesInfo,
	removeItemShoppingCart,
}) => {
	const { data, loading } = coursesInfo;
	let itemsComponent;

	if (loading)
		itemsComponent = (
			<div className='flex-c-c w-full p-1_5'>
				<Loader />
			</div>
		);
	else if (data.course_public_find_by_url_array.length > 0) {
		itemsComponent = data.course_public_find_by_url_array.map(
			(course: PublicCourse) => (
				<CartItem
					key={course._id}
					removeItemShoppingCart={removeItemShoppingCart}
					course={course}></CartItem>
			)
		);
	} else {
		itemsComponent = (
			<div className='flex-c-c w-full p-1_5'>
				<p className='text-center font-semibold'>
					No hay productos en el carrito
				</p>
			</div>
		);
	}

	return (
		<>
			<h1 className='text-center text-2xl font-semibold'>TU CARRITO</h1>
			<div className='w-full pt-2'>{itemsComponent}</div>
		</>
	);
};

export type CartItemProps = {
	course: PublicCourse;
	removeItemShoppingCart: (courseId: string) => Promise<void>;
};

const CartItem: FC<CartItemProps> = ({ course, removeItemShoppingCart }) => (
	<div className='flex-s-s relative p-0_5 pr-4 my-0_75 w-full bg-white dark:bg-gray-900 text-white-dark dark:text-white rounded-lg shadow-lg'>
		<ImgFallback
			className='h-7 mr-0_5'
			src={course.image}
			fallbackSrc='/static/android-chrome-192x192.png'
		/>
		<div className='flexcol-s-s'>
			<h4 className='p-0_25 text-lg font-semibold truncate-1-lines'>
				{course.title}
			</h4>
			<p className='p-0_25 truncate-3-lines text-sm'>{course.description}</p>
		</div>
		<div
			className='absolute top-0_5 right-0_5 cursor-pointer text-red-500'
			onClick={() => removeItemShoppingCart(course._id)}>
			<TrashIcon className='h-1_5' />
		</div>
		<div className='absolute right-1 bottom-0_5 flex-c-c'>
			<span className='font-semibold text-lg'>{course.price}€</span>
		</div>
	</div>
);

export default CheckoutItems;
