import Button from '@Components/generic/form/button/button';
import { AuthState } from '@Interfaces/states/browser-context.interface';
import { ShoppingCartState } from '@Interfaces/states/shoppingcart-context.interface';
import { AuthContext } from '@Lib/context/auth.context';
import { ShoppingCartContext } from '@Lib/context/shoppingcart.context';
import { FC, useContext } from 'react';

export type CourseHeadProps = {
	id: string;
	image: string;
	background: string;
	title: string;
	description: string;
};

const CourseHead: FC<CourseHeadProps> = ({
	id,
	image,
	background,
	title,
	description,
}) => {
	const { shoppingCart, addItemShoppingCart } = useContext(ShoppingCartContext);
	const { stateAuth } = useContext(AuthContext);

	const buttonProps = getButtonProps(stateAuth, shoppingCart, id);
	return (
		<div className='relative px-2 py-4 overflow-hidden'>
			<div
				style={{
					backgroundImage: background ? `url(${background})` : '',
				}}
				className='backgroundStyles absolute top-0 left-0 w-full h-full'
			/>
			<div className='container-xl flex flex-wrap px-1 relative z20'>
				<img
					className='w-3/12 max-w-15 max-h-15 xssm:max-w-12 xssm:max-h-12 xssm:w-full xssm:mx-auto'
					src={image}
				/>
				<div className='w-9/12 p-2 flexcol-s-s xssm:w-full xssm:flexcol-s-c'>
					<h1 className='text-3xl pb-1 text-white'>{title}</h1>
					<p className='text-white truncate-3-lines h-4_5 mb-2'>
						{description}
					</p>
					<Button
						className='font-semibold'
						disabled={buttonProps.disabled}
						kind='cta'
						onClick={() => addItemShoppingCart(id)}>
						{buttonProps.text}
					</Button>
				</div>
			</div>
		</div>
	);
};

const getButtonProps = (
	stateAuth: AuthState,
	shoppingCart: ShoppingCartState,
	id: string
) => {
	const buttonProps = { disabled: false, text: 'Añadir a la cesta' };

	if (shoppingCart.courses.includes(id)) buttonProps.text = 'Añadido';
	if (
		stateAuth.student?.coursesEnrolled &&
		stateAuth.student?.coursesEnrolled.filter(course => course.course === id)
			.length > 0
	)
		buttonProps.text = 'Adquirido';
	if (
		shoppingCart.courses.includes(id) ||
		(stateAuth.student?.coursesEnrolled &&
			stateAuth.student?.coursesEnrolled.filter(course => course.course === id)
				.length > 0)
	) {
		buttonProps.disabled = true;
	}

	return buttonProps;
};

export default CourseHead;
