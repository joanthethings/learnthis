//Courses
import { InfoIcon } from './courses/info-icon';
import { VideoIcon } from './courses/video-icon';

// Generic
import { CheckIcon } from './generic/check-icon';

// Logo
import { LogoTextIcon } from './logo/logo-text-icon';
import { LogoIcon } from './logo/logo-icon';

// Menu
import { AboutIcon } from './menu/about-icon';
import { CartIcon } from './menu/cart-icon';
import { CloseIcon } from './menu/close-icon';
import { ContactIcon } from './menu/contact-icon';
import { CoursesIcon } from './menu/courses-icon';
import { DownArrowIcon } from './menu/down-arrow-icon';
import { LeftArrowIcon } from './menu/left-arrow-icon';
import { LogoutIcon } from './menu/logout-icon';
import { MenuIcon } from './menu/menu-icon';
import { RightArrowIcon } from './menu/right-arrow-icon';
import { UserIcon } from './menu/user-icon';

// Profile
import { CalendarIcon } from './profile/calendar-icon';
import { EmailIcon } from './profile/email-icon';
import { HelpIcon } from './profile/help-icon';
import { KeyIcon } from './profile/key-icon';
import { OrdersIcon } from './profile/orders-icon';
import { PersonalDataIcon } from './profile/personal-data-icon';
import { UploadIcon } from './profile/upload-icon';
import { UserSettingsIcon } from './profile/user-settings-icon';

// Social
import { GithubIcon } from './social/github-icon';
import { GitlabIcon } from './social/gitlab-icon';
import { GoogleIcon } from './social/google-icon';

//// Exports

export {
	InfoIcon,
	VideoIcon,
	CheckIcon,
	LogoTextIcon,
	LogoIcon,
	AboutIcon,
	CartIcon,
	CloseIcon,
	ContactIcon,
	CoursesIcon,
	DownArrowIcon,
	LeftArrowIcon,
	LogoutIcon,
	MenuIcon,
	RightArrowIcon,
	UserIcon,
	CalendarIcon,
	EmailIcon,
	HelpIcon,
	KeyIcon,
	OrdersIcon,
	PersonalDataIcon,
	UploadIcon,
	UserSettingsIcon,
	GithubIcon,
	GitlabIcon,
	GoogleIcon,
};
