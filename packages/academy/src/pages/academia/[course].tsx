import { MainPaths } from '@Enums/paths/main-paths.enum';
import { RedirectConditions } from '@Enums/redirect-conditions.enum';
import { PublicFullCourse } from '@Interfaces/course/course.interface';
import { GSSProps } from '@Interfaces/props/gss-props.interface';
import { IRedirect } from '@Interfaces/redirect.interface';
import { createApolloClient } from '@Lib/apollo/apollo-client';
import { getJwtFromCookie } from '@Lib/login/jwt-cookie.utils';
import {
	isRequestSSR,
	loadAuthProps,
	serverRedirect,
} from '@Lib/utils/ssr.utils';
import { getThemeFromCookie } from '@Lib/utils/theme.utils';
import { decode } from 'jsonwebtoken';
import { GraphqlStudent } from 'learnthis-utils';
import { GetServerSideProps, GetServerSidePropsContext } from 'next';
import { FC } from 'react';

export type AcademyCourseProps = {
	course: PublicFullCourse;
};

const AcademyCourse: FC<AcademyCourseProps> = () => {
	return <></>;
};

const notAuthRedirect: IRedirect = {
	href: MainPaths.LOGIN,
	statusCode: 302,
	condition: RedirectConditions.REDIRECT_WHEN_USER_NOT_EXISTS,
	query: { returnTo: MainPaths.PROFILE },
};

export const getServerSideProps: GetServerSideProps = async (
	ctx: GetServerSidePropsContext
) => {
	const props: GSSProps = { lostAuth: false };
	const isSSR = isRequestSSR(ctx.req.url);

	const { cookie } = ctx.req.headers;
	const jwt = getJwtFromCookie(cookie);

	const apolloClient = createApolloClient();

	if (jwt) {
		if (isSSR) {
			const authProps = await loadAuthProps(ctx.res, jwt, apolloClient);

			if (authProps) props.authProps = authProps;
			else serverRedirect(ctx.res, notAuthRedirect);
		} else if (!decode(jwt)) props.lostAuth = true;
	}

	try {
		await apolloClient.query({
			query: GraphqlStudent.student_course_progress,
			variables: { courseUrl: ctx.params?.course },
			context: { headers: { Authorization: `Bearer ${jwt}` } },
		});
	} catch {
		if (isSSR)
			serverRedirect(ctx.res, {
				href: `${MainPaths.COURSES}/${ctx.params?.course}`,
				statusCode: 302,
			});
	}

	props.apolloCache = apolloClient.cache.extract();
	props.componentProps = { theme: getThemeFromCookie(ctx.req.headers.cookie) };

	return {
		props,
	};
};

export default AcademyCourse;
