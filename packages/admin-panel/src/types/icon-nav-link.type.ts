import { ComponentProps, FC } from 'react';

export type CustomNavLink = {
	to: string;
	exact: boolean;
	text: string;
};
