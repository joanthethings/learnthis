import ActiveSpan from '@Components/workers/active-span';
import { IStudent } from '@Interfaces/client.interface';
import { FC } from 'react';

type ClientRowProps = {
	client: IStudent;
};

const ClientRow: FC<ClientRowProps> = ({ client }) => {
	return (
		<div className='flex-c-c border-b border-gray-light dark:border-gray-dark py-0_5'>
			<div className='xssm:w-full mdlg:w-3/6'>
				<span className='text-white-dark dark:text-white'>{client._id}</span>
			</div>
			<div className='xssm:w-full mdlg:w-1/6 text-center'>
				<span className='text-white-dark dark:text-white'>
					{client.orders.length}
				</span>
			</div>
			<div className='xssm:w-full mdlg:w-1/6 text-center'>
				<span className='text-white-dark dark:text-white'>
					{client.coursesEnrolled.length}
				</span>
			</div>
			<div className='xssm:w-full mdlg:w-1/6 text-center'>
				<ActiveSpan active={client.active} />
			</div>
		</div>
	);
};

export default ClientRow;
