import { DownArrowIcon } from '@Components/icons/generic/arrow-icons';
import { InfoIcon } from '@Components/icons/lessons/info-icon';
import { VideoIcon } from '@Components/icons/lessons/video-icon';
import { LessonTypes } from '@Enums/course/lesson-types.enum';
import { MainParams, MainPaths } from '@Enums/paths/main-paths.enum';
import {
	ILesson,
	ILessonInfo,
	ILessonVideo,
} from '@Interfaces/lesson.interface';
import { ISection } from '@Interfaces/section.interface';
import { formatDuration } from '@Lib/utils/date.utils';
import { Dispatch, FC, SetStateAction, useState } from 'react';
import { Link } from 'react-router-dom';
import CoursePublishedSpan from '../course-published-span';

type CourseSectionsProps = {
	sections: ISection[];
	courseId: string;
};

const CourseSections: FC<CourseSectionsProps> = ({
	sections,
	courseId: courseId,
}) => {
	const [sectionState, setSectionState] = useState<number[]>([]);

	return (
		<div className='flexcol-c-c w-full px-1'>
			<h3 className='mb-2 text-black dark:text-white text-2xl font-semibold'>
				Secciones
			</h3>
			{sections.map((section, index) => (
				<SectionCourse
					key={index}
					section={section}
					index={index}
					sectionState={sectionState}
					setSectionState={setSectionState}
					courseId={courseId}
				/>
			))}
		</div>
	);
};

type SectionCourseProps = {
	section: ISection;
	index: number;
	sectionState: number[];
	setSectionState: Dispatch<SetStateAction<number[]>>;
	courseId: string;
};

const SectionCourse: FC<SectionCourseProps> = ({
	section,
	index,
	sectionState,
	setSectionState,
	courseId,
}) => {
	return (
		<div className='w-full shadow-lg mb-1 rounded-md box-border overflow-hidden'>
			<div className='bg-background dark:bg-background-dark py-1 px-1_5 w-full flex-s-c'>
				<div className='flex-s-c w-11/12'>
					<p className='mr-1 font-semibold text-lg text-black dark:text-white'>
						{index + 1}
					</p>
					<Link
						to={MainPaths.SECTION.replace(
							MainParams.COURSE_ID,
							courseId
						).replace(MainParams.SECTION_ID, section._id)}>
						<h4 className='font-semibold text-lg text-black dark:text-white'>
							{section.title}
						</h4>
					</Link>
				</div>
				<CoursePublishedSpan visibility={section.visibility} />
				<div
					className='w-1/12 cursor-pointer'
					onClick={() => changeSectionState(index, setSectionState)}>
					<DownArrowIcon
						className={`ml-0_5 w-1_5 text-black dark:text-white fill-current transform-gpu ${
							sectionState.includes(index) ? 'rotate-0' : '-rotate-90'
						}`}
					/>
				</div>
			</div>
			<div
				className={`w-full transition-all-eio-250 ${
					sectionState.includes(index)
						? 'opacity-1 max-h-30'
						: 'opacity-0 max-h-0'
				}`}>
				{section.lessons.map(
					(lesson: ILessonVideo | ILessonInfo, index: number) => (
						<LessonCourse key={index} lesson={lesson} />
					)
				)}
			</div>
		</div>
	);
};

type LessonCourseProps = {
	lesson: ILesson;
};

const LessonCourse: FC<LessonCourseProps> = ({ lesson }) => {
	let lessonType;

	if (lesson.__typename === LessonTypes.INFO) {
		lessonType = (
			<>
				<div className='flex-c-c'>
					<InfoIcon className='h-1_5 pr-1 text-black dark:text-white fill-current' />
					<Link to={MainPaths.LESSON.replace(MainParams.LESSON_ID, lesson._id)}>
						<h5 className='text-black dark:text-white'>{lesson.title}</h5>
					</Link>
				</div>
				<CoursePublishedSpan visibility={lesson.visibility} />
			</>
		);
	} else {
		const duration = formatDuration(lesson.duration);

		lessonType = (
			<>
				<div className='flex-c-c'>
					<VideoIcon className='h-1_5 pr-1 text-black dark:text-white fill-current' />
					<Link to={MainPaths.LESSON.replace(MainParams.LESSON_ID, lesson._id)}>
						<h5 className='text-black dark:text-white'>{lesson.title}</h5>
					</Link>
				</div>
				<div className='flex-c-c'>
					<CoursePublishedSpan visibility={lesson.visibility} />
					<p className='text-black dark:text-white ml-0_75'>{duration}</p>
				</div>
			</>
		);
	}

	return (
		<div className='px-2 py-0_75 bg-background dark:bg-background-dark flex-sb-c flex-wrap w-full'>
			{lessonType}
		</div>
	);
};

const changeSectionState = (
	index: number,
	setSectionState: Dispatch<SetStateAction<number[]>>
) =>
	setSectionState((oldState: number[]) => {
		if (oldState.includes(index))
			return oldState.filter(sectionIndex => sectionIndex !== index);
		else return [...oldState, index];
	});

export default CourseSections;
