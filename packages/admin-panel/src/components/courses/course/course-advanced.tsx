import { ApolloError, useMutation } from '@apollo/client';
import { BanIcon } from '@Components/icons/generic/ban-icon';
import { CheckCircleIcon } from '@Components/icons/generic/check-icon';
import Loader from '@Components/other/loader';
import { MainParams, MainPaths } from '@Enums/paths/main-paths.enum';
import { ApolloQueryRefetch } from '@Interfaces/apollo/apollo-refetch.types';
import { ICourse } from '@Interfaces/course.interface';
import { FC, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import { GraphqlCourse } from 'learnthis-utils';
import Button from '@Components/generic/form/button/button';

type CourseFormAdvancedProps = {
	course?: ICourse;
	refetch: ApolloQueryRefetch;
};

enum FormStates {
	BASE,
	CONFIRM_UNPUBLISH,
	CONFIRM_PUBLISH,
	CONFIRM_REMOVE_STEP_1,
	CONFIRM_REMOVE_STEP_2,
}

/**
 * Subpage component to perform advanced actions on the course.
 *
 * Authenticated: YES
 * Allowed roles: Admin
 *
 * @param props.course Course data
 * @param props.refetch Apollo course query refetch
 */
const CourseAdvanced: FC<CourseFormAdvancedProps> = ({ course, refetch }) => {
	const [formState, setFormState] = useState<FormStates>(FormStates.BASE);

	const {
		publishCourseMutation,
		unpublishCourseMutation,
		deleteCourseMutation,
		loading,
	} = getMutations(refetch, course);

	const confirmPopups: [string, () => void][] = [];
	confirmPopups[FormStates.CONFIRM_UNPUBLISH] = [
		`¿Está seguro de que desea retirar el curso '${course?.title}'?`,
		() => {
			unpublishCourseMutation();
			setFormState(FormStates.BASE);
		},
	];
	confirmPopups[FormStates.CONFIRM_PUBLISH] = [
		`¿Está seguro de que desea publicar el curso '${course?.title}'?`,
		() => {
			publishCourseMutation();
			setFormState(FormStates.BASE);
		},
	];
	confirmPopups[FormStates.CONFIRM_REMOVE_STEP_1] = [
		`¿Está seguro de que desea eliminar definitivamente  el curso '${course?.title}'?`,
		() => setFormState(FormStates.CONFIRM_REMOVE_STEP_2),
	];
	confirmPopups[FormStates.CONFIRM_REMOVE_STEP_2] = [
		'Esta acción no se puede deshacer, ¿está seguro?',
		() => {
			deleteCourseMutation();
			setFormState(FormStates.BASE);
		},
	];

	let advancedOptions;
	if (formState !== FormStates.BASE) {
		const popup = confirmPopups[formState];
		advancedOptions = (
			<div className='w-full max-w-20 mx-auto'>
				<p className='text-center mb-1_5 text-black dark:text-white'>
					{popup[0]}
				</p>
				<div className='flex-c-c'>
					<Button
						loading={loading}
						kind='border'
						className='mr-1'
						onClick={popup[1]}>
						Aceptar
					</Button>
					<Button
						kind='primary'
						className='ml-1'
						onClick={() => setFormState(FormStates.BASE)}>
						Cancelar
					</Button>
				</div>
			</div>
		);
	} else {
		advancedOptions = (
			<div className='w-full flex-c-c'>
				<div className='w-full max-w-25'>
					<h2 className='font-semibold mb-0_5 text-black dark:text-white'>
						{`${course?.visibility ? 'Retirar' : 'Publicar'} ${course?.title}`}
					</h2>
					<p className='text-black dark:text-white mb-1'>
						{course?.visibility
							? 'El curso se encuentra público. Si se retira, no será visible en la plataforma pública.'
							: 'El curso se encuentra retirado. Si se activa, será visible y se podrá adquirir en la plataforma pública.'}
					</p>
					{course?.visibility ? (
						<Button
							kind='red'
							onClick={() => setFormState(FormStates.CONFIRM_UNPUBLISH)}>
							Desactivar
						</Button>
					) : (
						<Button
							kind='green'
							onClick={() => setFormState(FormStates.CONFIRM_PUBLISH)}>
							Activar
						</Button>
					)}
					<hr className='m-1_5 border-gray-light dark:border-gray-dark' />
					<h2 className='font-semibold mb-0_5 text-black dark:text-white'>{`Eliminar el curso de ${course?.title}`}</h2>
					<p className='text-black dark:text-white'>
						Esta opción implica eliminar completamente el curso de la base de
						datos.
					</p>
					<p className='my-1 text-black dark:text-white'>
						Esta acción <strong>NO</strong> se puede deshacer.
					</p>
					<Button
						kind='border-red'
						onClick={() => setFormState(FormStates.CONFIRM_REMOVE_STEP_1)}>
						Eliminar permanentemente
					</Button>
				</div>
			</div>
		);
	}

	return (
		<div className='w-full flexcol-c-c'>
			<h3 className='mb-2 text-white-dark dark:text-white text-2xl font-semibold'>
				Opciones avanzadas
			</h3>
			{advancedOptions}
		</div>
	);
};

/**
 * Gets the mutations associated with the advanced options for the course.
 *
 * @param props.refetch Apollo course query refetch
 * @param props.course Course data
 */
const getMutations = (refetch: ApolloQueryRefetch, course?: ICourse) => {
	const history = useHistory();

	const options = {
		variables: { courseId: course?._id },
		onError: (error: ApolloError) => {
			toast.error(error.message);
		},
	};

	const [publishCourseMutation, { loading: enableLoading }] = useMutation(
		GraphqlCourse.course_admin_publish,
		{
			...options,
			onCompleted: () => {
				toast.success('Curso publicado');
				refetch();
			},
		}
	);
	const [unpublishCourseMutation, { loading: disableLoading }] = useMutation(
		GraphqlCourse.course_admin_unpublish,
		{
			...options,
			onCompleted: () => {
				toast.success('Curso retirado');
				refetch();
			},
		}
	);
	const [deleteCourseMutation, { loading: deleteLoading }] = useMutation(
		GraphqlCourse.course_admin_delete,
		{
			...options,
			onCompleted: () => {
				toast.success('Curso eliminado');
				history.push(MainPaths.COURSES_LIST);
			},
		}
	);

	return {
		publishCourseMutation,
		unpublishCourseMutation,
		deleteCourseMutation,
		loading: enableLoading || disableLoading || deleteLoading,
	};
};

export default CourseAdvanced;
