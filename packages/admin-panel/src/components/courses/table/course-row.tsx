import { EyeIcon } from '@Components/icons/generic/eye-icon';
import { MainParams, MainPaths } from '@Enums/paths/main-paths.enum';
import { ICourse } from '@Interfaces/course.interface';
import { FC } from 'react';
import { Link } from 'react-router-dom';
import CoursePhoto from '../course-photo';
import CoursePublishedSpan from '../course-published-span';

type CourseRowProps = {
	course: ICourse;
};

const CourseRow: FC<CourseRowProps> = ({ course }) => {
	return (
		<div className='flex-c-c flex-wrap border-b border-gray-light dark:border-gray-dark py-0_5'>
			<div className='w-full mdlg:w-5/12 flex-s-c px-1_5 xssm:pb-1'>
				<CoursePhoto
					className='h-2_25 w-2_25 rounded-full'
					title={course.title}
					photo={course.image}
				/>
				<div className='flexcol-c-s ml-1_5'>
					<span className='text-left text-black dark:text-white'>
						{course.title}
					</span>
				</div>
			</div>
			<div className='w-4/12 mdlg:w-2/12 text-center'>
				<CoursePublishedSpan visibility={course.visibility} />
			</div>
			<div className='w-4/12 mdlg:w-2/12 flex-c-c'>
				<span className='text-black dark:text-white'>{course.price}€</span>
				{course.compareAtPrice && (
					<span className='text-orange ml-0_25 line-through'>
						{course.compareAtPrice}€
					</span>
				)}
			</div>
			<div className='w-2/12 mdlg:w-2/12 text-center'>
				<span className='text-black dark:text-white'>
					{course.studentsCount}
				</span>
			</div>
			<div className='w-2/12 mdlg:w-1/12 flex-c-c'>
				<Link to={MainPaths.COURSE.replace(MainParams.COURSE_ID, course._id)}>
					<div className='h-2 w-2 flex-c-c rounded-full text-white bg-primary'>
						<EyeIcon className='h-1_5 w-1_5' />
					</div>
				</Link>
			</div>
		</div>
	);
};

export default CourseRow;
