import { MainPaths } from '@Enums/paths/main-paths.enum';
import { ComponentProps, FC } from 'react';
import { NavLink } from 'react-router-dom';

export type MenuItemProps = {
	title: string;
	href: string;
	icon: FC<ComponentProps<'svg'>>;
};

const MenuItem: FC<MenuItemProps> = ({ title, href, icon: Icon }) => (
	<NavLink
		to={href}
		exact={href === MainPaths.DASHBOARD}
		className='py-1 px-1_25 flex-s-c text-gray stroke-gray rounded-xl'
		activeClassName='text-white stroke-white bg-primary'>
		<Icon className='h-1_5 mr-1_5' />
		<span className='font-semibold'>{title}</span>
	</NavLink>
);

export default MenuItem;
