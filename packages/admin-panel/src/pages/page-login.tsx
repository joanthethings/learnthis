import { ApolloError, useLazyQuery } from '@apollo/client';
import Button from '@Components/generic/form/button/button';
import Input from '@Components/generic/form/input';
import { LogoTextIcon } from '@Components/icons/logo/logo-text-icon';
import LoginLayout from '@Components/layout/login.layout';
import { AlertMessages, FormMessages } from '@Enums/config/constants';
import { MainPaths } from '@Enums/paths/main-paths.enum';
import { ApolloLazyQuery } from '@Interfaces/apollo/apollo-query.types';
import { AuthContext } from '@Lib/context/auth.context';
import { withNoAuth } from '@Lib/hoc/withNoAuth';
import { useToggleDarkMode } from '@Lib/hooks/useToggleDarkMode';
import { Form, Formik, FormikConfig } from 'formik';
import { FormValidation, GraphqlWorker } from 'learnthis-utils';
import { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import { object as YupObject, string as YupString } from 'yup';

//#region TS

/**
 * Enum for form field names
 */
enum LoginFields {
	EMAIL = 'email',
	PASSWORD = 'password',
}

/**
 * Interface for form field values
 */
interface ILoginInput {
	[LoginFields.EMAIL]: string;
	[LoginFields.PASSWORD]: string;
}

//#endregion

/**
 * Page component to register a new worker.
 *
 * Authenticated: NO
 */
const PageLogin = () => {
	const { loginQuery, loading } = getLoginQuery();

	const formikConfig = getForm(loginQuery);
	useToggleDarkMode();

	return (
		<LoginLayout>
			<div className='h-screen w-screen flex-c-c base-login z10'>
				<div className='flex-col flex-c-c max-w-50 shadow-lg p-2 mdlg:rounded-lg mdlg:bg-white w-full mdlg:flex-row xssm:h-screen xssm:text-white'>
					<div className='p-2 w-full md:w-1/2 flex-c-c'>
						<LogoTextIcon className='h-4' />
					</div>
					<div className='p-2 w-full md:w-1/2 flex-c-c'>
						<Formik {...formikConfig}>
							<Form className='w-full'>
								<h1 className='mb-2 text-xl font-semibold mdlg:text-gray-700 xssm:text-center'>
									Login
								</h1>
								<Input
									name={LoginFields.EMAIL}
									className='mt-1'
									type='email'
									label='Email'
								/>
								<Input
									name={LoginFields.PASSWORD}
									className='mt-1'
									type='password'
									label='Contraseña'
								/>
								<Button
									loading={loading}
									className='mt-2 p-0_75 w-full'
									type='submit'
									kind='primary'>
									Iniciar sesión
								</Button>
								<p className='mt-2'>
									<a className='text-sm font-medium text-primary hover:underline'>
										Forgot your password?
									</a>
								</p>
							</Form>
						</Formik>
					</div>
				</div>
			</div>
		</LoginLayout>
	);
};

/**
 * Gets the login graphql query.
 */
const getLoginQuery = () => {
	const { login } = useContext(AuthContext);

	const history = useHistory();
	const query = GraphqlWorker.worker_login;

	const [loginQuery, { loading }] = useLazyQuery(query, {
		onCompleted: data => {
			toast.success(AlertMessages.WELCOME);
			login(data.worker_login.token, data.worker_login.worker);
			// If comes from redirect
			if (history.action === 'PUSH') history.goBack();
			else history.push(MainPaths.DASHBOARD);
		},
		onError: (error: ApolloError) => {
			toast.error(error.message || AlertMessages.SERVER_ERROR);
		},
	});

	return { loginQuery, loading };
};

/**
 * Gets the formik data to build the form.
 * @param loginQuery Graphql query
 */
const getForm = (loginQuery: ApolloLazyQuery): FormikConfig<ILoginInput> => {
	const initialValues = {
		[LoginFields.EMAIL]: '',
		[LoginFields.PASSWORD]: '',
	};

	const validationSchema = YupObject().shape({
		[LoginFields.EMAIL]: YupString()
			.test('login.email', FormMessages.EMAIL_ERROR, (value: any) =>
				FormValidation.emailValidation(value || '')
			)
			.required(FormMessages.EMAIL_REQUIRED),
		[LoginFields.PASSWORD]: YupString()
			.test('login.password', FormMessages.PASSWORD_ERROR, (value: any) =>
				FormValidation.passwordValidation(value || '')
			)
			.required(FormMessages.PASSWORD_REQUIRED),
	});

	const onSubmit = (values: ILoginInput) => {
		loginQuery({
			variables: {
				input: {
					email: values[LoginFields.EMAIL],
					password: values[LoginFields.PASSWORD],
				},
			},
		});
	};

	return {
		initialValues,
		validationSchema,
		onSubmit,
		validateOnChange: false,
	};
};

export default withNoAuth(PageLogin);
