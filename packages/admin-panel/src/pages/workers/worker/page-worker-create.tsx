import { ApolloError, useMutation } from '@apollo/client';
import Button from '@Components/generic/form/button/button';
import Input from '@Components/generic/form/input';
import Select from '@Components/generic/form/select/select';
import SelectItem from '@Components/generic/form/select/select-item';
import SectionLayout from '@Components/layout/section.layout';
import { AlertMessages, FormMessages } from '@Enums/config/constants';
import { MainPaths } from '@Enums/paths/main-paths.enum';
import { WorkerRoles } from '@Enums/worker/worker-roles.enum';
import { ApolloMutation } from '@Interfaces/apollo/apollo-query.types';
import { Form, Formik, FormikConfig } from 'formik';
import { FormValidation, GraphqlWorker } from 'learnthis-utils';
import { Dispatch, FC, SetStateAction, useState } from 'react';
import { Prompt, useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import { object as YupObject, ref as YupRef, string as YupString } from 'yup';

//#region TS

/**
 * Enum for form field names
 */
enum CreateWorkerField {
	EMAIL = 'reg_email',
	NAME = 'reg_name',
	SURNAME = 'reg_surname',
	PASSWORD = 'reg_password',
	CONFIRM_PASSWORD = 'reg_conf_password',
	ROLE = 'reg_roles',
}

/**
 * Interface for form field values
 */
interface ICreateWorkerInput {
	[CreateWorkerField.EMAIL]: string;
	[CreateWorkerField.NAME]: string;
	[CreateWorkerField.SURNAME]: string;
	[CreateWorkerField.PASSWORD]: string;
	[CreateWorkerField.CONFIRM_PASSWORD]: string;
	[CreateWorkerField.ROLE]: WorkerRoles | string;
}

//#endregion

/**
 * Page component to register a new worker.
 *
 * Authenticated: YES
 * Allowed roles: Admin
 */
const PageWorkerCreate: FC = () => {
	const [isDone, setIsDone] = useState<boolean>(false);

	const { registerUserMutation, loading } = getRegisterUserMutation(setIsDone);

	const formikConfig = getForm(registerUserMutation);

	return (
		<SectionLayout title='Usuarios'>
			<div className='container-sm m-1_5 bg-white dark:bg-white-dark p-1_5 rounded-2xl'>
				<Formik {...formikConfig}>
					{({ dirty }) => (
						<Form className='w-full max-w-24 mx-auto'>
							<Prompt
								when={dirty && !isDone}
								message='¿Estás seguro de que deseas salir? Perderás todos los cambios'
							/>
							<h1 className='p-1 font-semibold text-xl text-center text-black dark:text-white'>
								Registro de usuario
							</h1>
							<Input
								name={CreateWorkerField.EMAIL}
								className='mt-1'
								type='email'
								label='Email'
							/>
							<Input
								name={CreateWorkerField.NAME}
								className='mt-1'
								type='text'
								label='Nombre'
							/>
							<Input
								name={CreateWorkerField.SURNAME}
								className='mt-1'
								type='text'
								label='Apellidos'
							/>
							<Input
								name={CreateWorkerField.PASSWORD}
								className='mt-1'
								type='password'
								label='Contraseña'
							/>
							<Input
								name={CreateWorkerField.CONFIRM_PASSWORD}
								className='mt-1'
								type='password'
								label='Confirmar contraseña'
							/>
							<Select
								name={CreateWorkerField.ROLE}
								className='mt-1 w-full'
								label='Rol'
								placeholder='Seleccione un rol...'>
								<SelectItem value={WorkerRoles.ADMIN}>
									{WorkerRoles.ADMIN}
								</SelectItem>
								<SelectItem value={WorkerRoles.TEACHER}>
									{WorkerRoles.TEACHER}
								</SelectItem>
							</Select>
							<Button
								loading={loading}
								className='mt-2 p-0_75 w-10'
								type='submit'
								kind='primary'>
								Registrar
							</Button>
						</Form>
					)}
				</Formik>
			</div>
		</SectionLayout>
	);
};

/**
 * Gets the graphql mutation to register a new worker.
 */
const getRegisterUserMutation = (
	setIsDone: Dispatch<SetStateAction<boolean>>
) => {
	const history = useHistory();

	const [registerUserMutation, { loading }] = useMutation(
		GraphqlWorker.worker_register,
		{
			onCompleted: () => {
				toast.success(AlertMessages.REGISTER_USER_SUCCESS);
				setIsDone(true);
				history.push(MainPaths.WORKERS_LIST);
			},
			onError: (error: ApolloError) => {
				toast.error(error.message || AlertMessages.SERVER_ERROR);
			},
		}
	);

	return { registerUserMutation, loading };
};

/**
 * Gets the formik data to build the form.
 * @param {ApolloMutation} registerUserMutation Graphql mutation
 */
const getForm = (
	registerUserMutation: ApolloMutation
): FormikConfig<ICreateWorkerInput> => {
	const initialValues = {
		[CreateWorkerField.EMAIL]: '',
		[CreateWorkerField.NAME]: '',
		[CreateWorkerField.SURNAME]: '',
		[CreateWorkerField.PASSWORD]: '',
		[CreateWorkerField.CONFIRM_PASSWORD]: '',
		[CreateWorkerField.ROLE]: '',
	};

	const validationSchema = YupObject().shape({
		[CreateWorkerField.EMAIL]: YupString()
			.test('register.email', FormMessages.EMAIL_ERROR, (value: any) =>
				FormValidation.emailValidation(value || '')
			)
			.required(FormMessages.EMAIL_REQUIRED),
		[CreateWorkerField.NAME]: YupString()
			.test('register.name', FormMessages.NAME_ERROR, (value: any) =>
				FormValidation.nameValidation(value || '')
			)
			.required(FormMessages.NAME_REQUIRED),
		[CreateWorkerField.SURNAME]: YupString()
			.test('register.surname', FormMessages.SURNAME_ERROR, (value: any) =>
				FormValidation.nameValidation(value || '')
			)
			.required(FormMessages.SURNAME_REQUIRED),
		[CreateWorkerField.PASSWORD]: YupString()
			.test('register.password', FormMessages.PASSWORD_ERROR, (value: any) =>
				FormValidation.passwordValidation(value || '')
			)
			.required(FormMessages.PASSWORD_REQUIRED),
		[CreateWorkerField.CONFIRM_PASSWORD]: YupString()
			.oneOf([YupRef(CreateWorkerField.PASSWORD)], FormMessages.PASSWORD_CHECK)
			.required(FormMessages.CONFIRM_PASSWORD_REQUIRED),
		[CreateWorkerField.ROLE]: YupString()
			.required(FormMessages.CONFIRM_PASSWORD_REQUIRED)
			.oneOf(Object.keys(WorkerRoles)),
	});

	const onSubmit = (values: ICreateWorkerInput) => {
		registerUserMutation({
			variables: {
				input: {
					email: values[CreateWorkerField.EMAIL],
					password: values[CreateWorkerField.PASSWORD],
					name: values[CreateWorkerField.NAME],
					surname: values[CreateWorkerField.SURNAME],
					roles: values[CreateWorkerField.ROLE],
				},
			},
		});
	};

	return {
		initialValues,
		validationSchema,
		onSubmit,
		validateOnChange: false,
	};
};

export default PageWorkerCreate;
