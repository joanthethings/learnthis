import CourseHeader from '@Components/courses/course/course-header';
import CourseSections from '@Components/courses/course/course-sections';
import { withAuth } from '@Lib/hoc/withAuth';
import { FC } from 'react';
import { CourseWrapperProps } from 'src/types/props/with-course-props.type';

/**
 * Page component to view the profile of a specific user and perform advanced options on it.
 *
 * Authenticated: YES
 * Allowed roles: Admin
 */
const PageCourseMain: FC<CourseWrapperProps> = ({ course, refetch }) => (
	<>
		<CourseHeader course={course} refetch={refetch} />
		<CourseSections
			sections={course?.sections || []}
			courseId={course?._id || ''}
		/>
	</>
);

export default withAuth(PageCourseMain);
