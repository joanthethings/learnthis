import { FC, lazy } from 'react';
import { GraphqlCourse } from 'learnthis-utils';
import { ApolloError, useQuery } from '@apollo/client';
import { toast } from 'react-toastify';
import { Redirect, Route, Switch, useParams } from 'react-router-dom';
import { MainPaths, UrlParams } from '@Enums/paths/main-paths.enum';
import { ICourse } from '@Interfaces/course.interface';
import { AlertMessages } from '@Enums/config/constants';
import CourseLayout from '@Components/layout/course.layout';
import { NavTypes } from '@Enums/course/nav.types.enum';
import { withSuspense } from '@Lib/hoc/withSuspense';
import { SectionWrapperProps } from '@Types/props/with-section-props.type';
import SectionHeader from '@Components/courses/section/section-header';

//#region Lazy components

const SectionMain = lazy(
	() => import('@Pages/courses/section/page-section-main')
);
const SectionData = lazy(
	() => import('@Pages/courses/section/page-section-data')
);
const SectionLessons = lazy(
	() => import('@Pages/courses/section/page-section-lessons')
);
const SectionAdvanced = lazy(
	() => import('@Pages/courses/section/page-section-advanced')
);

//#endregion

const RouterSection: FC = () => {
	const { courseId, sectionId } = useParams<UrlParams>();
	const { data, loading, error, refetch } = getCourse(courseId);

	if (error) return <Redirect to={MainPaths.NOT_FOUND} />;

	const course =
		!loading && data ? (data.course_admin_find_by_id as ICourse) : null;

	let sectionIndex = null;
	let section = null;

	if (course) {
		sectionIndex = course.sections.findIndex(i => {
			console.log(i._id === sectionId);
			return i._id === sectionId;
		});

		section = course.sections[sectionIndex];

		if (!section) {
			toast.error(AlertMessages.SECTION_NOT_FOUND);
			return <Redirect to={MainPaths.NOT_FOUND} />;
		}
	}

	const props: SectionWrapperProps = {
		course,
		section,
		sectionId,
		refetch,
	};

	const routes = [
		{
			exact: true,
			path: MainPaths.SECTION,
			component: withSuspense(SectionMain, props),
		},
		{
			exact: true,
			path: MainPaths.SECTION_DATA,
			component: withSuspense(SectionData, props),
		},
		{
			exact: true,
			path: MainPaths.SECTION_LESSONS,
			component: withSuspense(SectionLessons, props),
		},
		{
			exact: true,
			path: MainPaths.SECTION_ADVANCED,
			component: withSuspense(SectionAdvanced, props),
		},
	];

	return (
		<CourseLayout title='Sección' type={NavTypes.SECTION} course={course}>
			<SectionHeader
				section={sectionIndex || 1}
				title={section?.title}
				visibility={section?.visibility}
			/>
			<Switch>
				{routes.map((route, i) => (
					<Route
						key={i}
						path={route.path}
						exact={route.exact}
						component={route.component}
					/>
				))}
			</Switch>
		</CourseLayout>
	);
};

const getCourse = (courseId: string) => {
	const { data, loading, error, refetch } = useQuery(
		GraphqlCourse.course_admin_find_by_id,
		{
			variables: {
				courseId,
			},
			onError: (error: ApolloError) => {
				toast.error(error.message);
			},
		}
	);

	return { data, loading, refetch, error };
};

export default RouterSection;
