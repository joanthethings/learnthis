import { ApolloProvider } from '@apollo/client';
import PageWithMenu from '@Components/menu/page-with-menu';
import { toastSettings } from '@Config/toast.settings';
import { MainPaths } from '@Enums/paths/main-paths.enum';
import { IResolutionState } from '@Interfaces/states/resolution-state.interface';
import { AuthContext } from '@Lib/context/auth.context';
import { ResolutionContext } from '@Lib/context/resolution.context';
import { withSuspense } from '@Lib/hoc/withSuspense';
import { useAuthAndApollo } from '@Lib/hooks/useAuthAndApollo.hook';
import { getResolution } from '@Lib/utils/resolution.utils';
import AppRoutes from '@Pages/_app-routes';
import '@Styles/index.css';
import { FC, lazy, useCallback, useEffect, useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';

const Login = lazy(() => import('@Pages/page-login'));

/**
 * Main App component
 */
const App: FC = () => {
	const { resolution, isMobile } = useResolution();
	const {
		apolloClient,
		authState,
		login,
		logout,
		updateUser,
	} = useAuthAndApollo();

	return (
		<>
			<ResolutionContext.Provider value={{ resolution, isMobile }}>
				<AuthContext.Provider value={{ authState, login, logout, updateUser }}>
					<ApolloProvider client={apolloClient}>
						<BrowserRouter>
							<Switch>
								<Route
									path={MainPaths.LOGIN}
									exact
									component={withSuspense(Login)}
								/>
								<PageWithMenu>
									<AppRoutes />
								</PageWithMenu>
							</Switch>
						</BrowserRouter>
					</ApolloProvider>
				</AuthContext.Provider>
			</ResolutionContext.Provider>
			<ToastContainer {...toastSettings} />
		</>
	);
};

/**
 * Hook to handle screen resolution state and its associated resize events
 */
const useResolution = (): IResolutionState => {
	const [resolutionState, setResolutionState] = useState<IResolutionState>(
		getResolution()
	);

	const handleResize = useCallback(() => {
		const newResolutionState = getResolution();
		if (newResolutionState.resolution !== resolutionState.resolution) {
			setResolutionState(newResolutionState);
		}
	}, [resolutionState]);

	useEffect(() => {
		window.addEventListener('resize', handleResize);

		return () => {
			window.removeEventListener('resize', handleResize);
		};
	}, [resolutionState]);

	return resolutionState;
};

export default App;
