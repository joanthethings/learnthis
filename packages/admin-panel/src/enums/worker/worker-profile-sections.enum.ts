export enum WorkerProfileSections {
	SUMMARY,
	DATA,
	COURSES,
	ADVANCED,
}
