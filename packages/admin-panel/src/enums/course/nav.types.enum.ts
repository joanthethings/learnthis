export enum NavTypes {
	COURSE = 'COURSE',
	SECTION = 'SECTION',
	LESSON = 'LESSON',
}
