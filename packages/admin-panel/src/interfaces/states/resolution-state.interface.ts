import { ResolutionBreakpoints } from '@Enums/config/resolution-breakpoint.enum';

export interface IResolutionState {
	resolution: ResolutionBreakpoints;
	isMobile: boolean;
}
