import { render } from 'react-dom';
import App from '@Pages/_app';

const rootDiv = document.getElementById('root');

render(<App />, rootDiv);
