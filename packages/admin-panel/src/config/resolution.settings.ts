export const ResolutionBreakpointValues = {
	XS: 480,
	SM: 768,
	MD: 1024,
};
