import Loader from '@Components/other/loader';
import { FC, ReactElement, Suspense } from 'react';

export const withSuspense = (
	WrappedComponent: FC<any>,
	props: any = {},
	FallbackComponent: ReactElement | null = <Loader />
) => () => {
	return (
		<Suspense fallback={FallbackComponent}>
			<WrappedComponent {...props} />
		</Suspense>
	);
};
