import useDarkMode from 'use-dark-mode';

export const useToggleDarkMode = () => {
	const { value: hasActiveDarkMode, toggle: toggleDarkMode } = useDarkMode(
		localStorage.theme === 'dark',
		{
			element: document.documentElement,
			classNameDark: 'dark',
			classNameLight: 'light',
			storageKey: 'theme',
		}
	);

	return { hasActiveDarkMode, toggleDarkMode };
};
