import { ResolutionBreakpoints } from '@Enums/config/resolution-breakpoint.enum';
import { createContext } from 'react';

interface IResolutionContext {
	resolution: ResolutionBreakpoints;
	isMobile: boolean;
}

export const ResolutionContext = createContext<IResolutionContext>({} as any);
