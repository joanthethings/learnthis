const colors = {
	primary: {
		DEFAULT: '#6C5DD3',
		light: '#b794f4',
		medium: '#805ad5',
		dark: '#553c9a',
	},
	orange: {
		DEFAULT: '#FF754C',
	},
	red: {
		DEFAULT: '#FF3636',
	},
	green: {
		DEFAULT: '#7FBA7A',
	},
	yellow: {
		DEFAULT: '#FFCE73',
	},
	pink: {
		DEFAULT: '#FFA2C0',
	},
	blue: {
		DEFAULT: '#588EE0',
		light: '#AEDDEB',
	},
	gray: {
		DEFAULT: '#808191',
		light: '#E4E4E4',
		dark: '#383B44',
	},
	black: {
		DEFAULT: '#11142D',
	},
	white: {
		DEFAULT: '#FFFFFF',
		dark: '#1C2025',
	},
	background: {
		DEFAULT: '#F7F7F9',
		dark: '#15171B',
		transparent: '#00000000',
	},
};

module.exports = {
	purge: ['./src/**/*.{ts,tsx}'],
	theme: {
		screens: {
			xs: { max: '480px' },
			sm: { min: '481px', max: '768px' },
			md: { min: '769px', max: '1024px' },
			lg: { min: '1025px' },
			xssm: { max: '768px' },
			smmd: { min: '481px', max: '1024px' },
			mdlg: { min: '769px' },
		},
		rotate: {
			'-180': '-180deg',
			'-90': '-90deg',
			'-45': '-45deg',
			0: '0',
			45: '45deg',
			90: '90deg',
			135: '135deg',
			180: '180deg',
			270: '270deg',
		},
		colors,
		fill: colors,
		stroke: colors,
	},
	darkMode: 'class',
	variants: {
		extend: {
			borderWidth: ['disabled'],
			borderColor: ['responsive', 'focus'],
			backgroundColor: ['responsive', 'hover', 'disabled'],
			cursor: ['disabled'],
			textColor: ['responsive', 'hover', 'disabled'],
			fill: ['dark'],
		},
	},
	plugins: [...require('tailwindcssdu')],
};
