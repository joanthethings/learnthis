import { BadRequestException } from '@nestjs/common';
import { ObjectIDPipe } from './objectid.pipe';

describe('ObjectIdPipe', () => {
	let pipe: ObjectIDPipe;

	beforeAll(() => (pipe = new ObjectIDPipe()));

	it('ObjectId válido', () => {
		const objectID = '5fe760a73efa5c7bee0fe68f';
		expect(pipe.transform(objectID)).toBe(objectID);
	});

	it('ObjectId inválido', () => {
		const objectID = '5fe760a73efa5c7bee0fe68';
		expect(() => pipe.transform(objectID)).toThrow(BadRequestException);
	});
});
