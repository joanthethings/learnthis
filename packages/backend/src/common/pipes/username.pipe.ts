import { StudentErrors } from '@Students/enums/student-errors.enum';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { FormValidation } from 'learnthis-utils';

/**
 * Validates if username has a valid format.
 */
@Injectable()
export class UsernamePipe implements PipeTransform {
	/**
	 * Validation pipe.
	 * @param value Username
	 * @returns Same username
	 */
	transform(value: string): string {
		if (!FormValidation.usernameValidation(value))
			throw new BadRequestException(StudentErrors.FORMAT_USERNAME);
		return value;
	}
}
