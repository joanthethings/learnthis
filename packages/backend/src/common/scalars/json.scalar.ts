import { CustomScalar, Scalar } from '@nestjs/graphql';
import { Kind } from 'graphql';

@Scalar('JSONObject', type => Object)
export class JSONObjectScalar implements CustomScalar<object, object> {
	description = 'JSONObject custom scalar type';

	parseValue(value: object): object {
		return value;
	}

	serialize(value: object): object {
		return value;
	}

	parseLiteral(ast: any): object {
		if (ast.kind === Kind.OBJECT) {
			return new Object(ast.value);
		}
		return null;
	}
}
