import { ObjectID } from '@Common/types/object-id.type';

export class IJob {
	_id: ObjectID;
	name: string;
	type: string;
	priority: number;
	nextRunAt: string;
	lastModifiedBy: string;
	lockedAt: string;
	lastRunAt: string;
	lastFinishedAt: string;
	data: Object;
}
