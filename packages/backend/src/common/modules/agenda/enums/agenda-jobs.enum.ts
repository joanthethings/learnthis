export enum AgendaJobs {
	ActivationMail = 'ActivationMail',
	ForgotPasswordMail = 'ForgotPasswordMail',
	UploadVideoMux = 'UploadVideoMux',
	ImageProcess = 'ImageProcess',
}
