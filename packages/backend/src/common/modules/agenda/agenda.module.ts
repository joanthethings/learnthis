import {
	Controller,
	forwardRef,
	Get,
	MiddlewareConsumer,
	Module,
	NestModule,
} from '@nestjs/common';
import { AgendaService } from './services/agenda.service';
import Agendash from 'agendash2';
import { MuxService } from './services/mux.service';
import { BasicAuthMiddleware } from '@Common/auth/basic-auth.middleware';
import { WorkersModule } from '@Workers/workers.module';
import { JobResolver } from './resolvers/job.resolver';
import { FileProcessService } from './services/file-process.service';

@Module({
	imports: [forwardRef(() => WorkersModule)],
	providers: [AgendaService, MuxService, JobResolver, FileProcessService],
	exports: [AgendaService, MuxService],
})
export class AgendaModule implements NestModule {
	/**
	 * Configure Angenda Dashboard
	 *
	 * @param consumer
	 */
	configure(consumer: MiddlewareConsumer) {
		consumer
			.apply(BasicAuthMiddleware, Agendash(AgendaService.getInstance()))
			.forRoutes('agenda/admin');
	}
}
