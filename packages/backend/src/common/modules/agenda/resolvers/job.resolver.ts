import { PaginateDto } from '@Common/dto/paginate.dto';
import { ObjectIDPipe } from '@Common/pipes/objectid.pipe';
import { ObjectID } from '@Common/types/object-id.type';
import { NotFoundException, UseGuards } from '@nestjs/common';
import { Args, ID, Query, Resolver } from '@nestjs/graphql';
import { WorkerGqlAuthGuard } from '@Workers/auth/worker-gql-auth.guard';
import { Job, JobPaginated } from '../gql-types/job.gqltype';
import { AgendaService } from '../services/agenda.service';

@Resolver(() => Job)
export class JobResolver {
	constructor(private readonly agendaService: AgendaService) {}

	/**
	 * Jobs list query with pagination
	 *
	 * @param paginate
	 */
	@Query(() => JobPaginated)
	@UseGuards(WorkerGqlAuthGuard)
	job_find(
		@Args('paginate', { type: () => PaginateDto, nullable: true })
		paginate: PaginateDto
	) {
		return this.agendaService.getJobs(
			(paginate && paginate.offset) || 0,
			(paginate && paginate.limit) || 10
		);
	}

	/**
	 * Job find by id query
	 *
	 * @param jobId
	 */
	@Query(() => Job)
	@UseGuards(WorkerGqlAuthGuard)
	async job_find_by_id(
		@Args('jobId', { type: () => ID }, ObjectIDPipe) jobId: ObjectID
	) {
		const job = await this.agendaService.getJob(jobId);
		if (!job) throw new NotFoundException();
		return job;
	}
}
