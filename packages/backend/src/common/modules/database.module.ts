import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule, MongooseModuleOptions } from '@nestjs/mongoose';
import { Env } from '@Common/enums/env.enum';

/**
 * Module that manages database connection through Mongoose.
 */
export const DatabaseModule = MongooseModule.forRootAsync({
	imports: [ConfigModule],
	useFactory: (configService: ConfigService): MongooseModuleOptions => ({
		uri: configService.get(Env.DATABASE_URI),
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false,
		useCreateIndex: true,
	}),
	inject: [ConfigService],
});
