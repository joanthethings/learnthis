import { Type } from '@nestjs/common';
import { Field, Int, ObjectType } from '@nestjs/graphql';

/**
 * Creates a Graphql's object type for paginated element of a given type.
 * @param TItemClass Type of paginated objects
 */
export default function Paginated<TItem>(TItemClass: Type<TItem>): any {
	const { name } = TItemClass;

	/**
	 * Graphql type: Paginated elements
	 */
	@ObjectType(`${name}Paginated`, { isAbstract: true })
	class _Paginated<TItem> {
		/** Result items */
		@Field(() => [TItemClass!])
		data!: TItem[];

		/** Number of skipped elements */
		@Field(() => Int)
		offset!: number;

		/** Number of elements returned */
		@Field(() => Int)
		limit!: number;

		/** Counter of total elements in table */
		@Field(() => Int)
		total!: number;
	}

	return _Paginated;
}
