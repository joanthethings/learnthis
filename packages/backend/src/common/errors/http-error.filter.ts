import {
	ArgumentsHost,
	BadRequestException,
	Catch,
	ExceptionFilter,
	HttpException,
	HttpStatus,
	InternalServerErrorException,
	Logger,
} from '@nestjs/common';
import { GqlContextType } from '@nestjs/graphql';
import { Response } from 'express';
import { Error as MongoError } from 'mongoose';
import { TimeoutError } from 'rxjs';
import { CommonErrors } from '@Common/enums/common-errors.enum';

/** Types of caught exceptions */
enum ExceptionTypes {
	/** From GraphQL requests */
	GRAPHQL = 'graphql',
	/** From REST requests */
	HTTP = 'http',
}

/**
 * Catches all the exceptions that occur during the execution of a request, and decides the format in which they should be sent to the client.
 */
@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
	/**
	 * Error delegate function
	 *
	 * @param exception Catched exception
	 * @param host Methods to get arguments from execution context
	 */
	catch(
		exception:
			| HttpException
			| MongoError
			| TimeoutError
			| Error
			| MongoError.ValidationError,
		host: ArgumentsHost
	): void {
		const type = host.getType<GqlContextType>();

		if (type === ExceptionTypes.GRAPHQL) {
			if (exception instanceof MongoError.ValidationError) {
				throw new BadRequestException();
			} else if (
				(exception instanceof MongoError ||
					exception instanceof TimeoutError ||
					exception instanceof Error) &&
				!(exception instanceof HttpException)
			) {
				//TODO Logging
				//const gqlHost = GqlArgumentsHost.create(host);
				Logger.error(exception, exception.stack, 'Error Filter');
				throw new InternalServerErrorException(CommonErrors.INTERNAL_ERROR);
			}
		} else if (type === ExceptionTypes.HTTP) {
			const ctx = host.switchToHttp();
			const response = ctx.getResponse<Response>();
			if (exception instanceof HttpException) {
				let statusCode = exception.getStatus();
				const r = <any>exception.getResponse();
				response.status(statusCode).json(r);
			} else if (exception['code'] === 'ENOENT') {
				response.status(HttpStatus.NOT_FOUND).json();
			} else {
				Logger.error(exception, exception.stack, 'Error Filter');
				response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
					message: CommonErrors.INTERNAL_ERROR,
					statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
				});
			}
		}
	}
}
