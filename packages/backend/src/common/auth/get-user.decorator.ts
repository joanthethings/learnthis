import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { IStudentDoc } from '@Students/interfaces/student-document.interface';
import { IWorkerDoc } from '@Workers/interfaces/worker-document.interface';

/**
 * GQL decorator for get auth user
 */
export const GetGqlAuthUser = createParamDecorator(
	(_, ctx: GqlExecutionContext): IStudentDoc | IWorkerDoc =>
		GqlExecutionContext.create(ctx).getContext().req.user
);

/**
 * Rest decorator for get auth user
 */
export const GetRestAuthUser = createParamDecorator(
	(data: unknown, ctx: ExecutionContext): IStudentDoc | IWorkerDoc => {
		const request = ctx.switchToHttp().getRequest();
		return request.user;
	}
);
