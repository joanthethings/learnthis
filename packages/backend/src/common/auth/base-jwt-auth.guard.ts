import { CommonErrors } from '@Common/enums/common-errors.enum';
import { ObjectID } from '@Common/types/object-id.type';
import { ObjectIdTokenPayload } from '@Common/types/objectid-token-payload.type';
import {
	CanActivate,
	ExecutionContext,
	UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

/**
 * Base guard to extract user from the request context, and set it to req.user.
 */
export abstract class BaseJwtAuthGuard implements CanActivate {
	/**
	 * Dependency injection
	 * @param jwtservice JWT service
	 * @param configService Config service
	 */
	constructor(protected readonly jwtservice: JwtService) {}

	/** Gets JWT secret */
	abstract getSecret(): string;

	/**
	 * Get HTTP request from context.
	 * @param context Execution context
	 */
	abstract getRequest(context: ExecutionContext): any;

	/**
	 * Sets user to request.user
	 * @param userId User's ObjectId
	 * @param req HTTP request
	 */
	abstract setUserToRequest(userId: ObjectID, req: any): Promise<void>;

	/**
	 * Extracts user from JWT token and adds it to req.user
	 * @param context Execution context
	 */
	async canActivate(context: ExecutionContext) {
		const req = this.getRequest(context);

		const bearerToken: string = req.headers.authorization;
		if (!bearerToken)
			throw new UnauthorizedException(CommonErrors.UNAUTHORIZED);

		const token = bearerToken.replace('Bearer ', '');

		try {
			const payload: ObjectIdTokenPayload = await this.jwtservice.verifyAsync(
				token,
				{
					secret: this.getSecret(),
				}
			);
			await this.setUserToRequest(payload.id, req);
		} catch (error) {
			throw new UnauthorizedException(CommonErrors.UNAUTHORIZED);
		}

		return true;
	}
}
