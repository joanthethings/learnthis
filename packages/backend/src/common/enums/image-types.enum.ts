/**
 * Allowed upload image types
 */
export enum ImageTypes {
	JPG = 'jpg',
	JPEG = 'jpeg',
	PNG = 'png',
}
