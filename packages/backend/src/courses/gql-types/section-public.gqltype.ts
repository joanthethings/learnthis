import { ObjectType } from '@nestjs/graphql';
import { LessonPublic } from './lesson-public.gqltype';

/**
 * Graphql type: Section in a course that only contains data that can be sent to the student.
 */
@ObjectType()
export class SectionPublic {
	title: string;
	description?: string;
	visibility: boolean;
	lessons: LessonPublic[];
}
