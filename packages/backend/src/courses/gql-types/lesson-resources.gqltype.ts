import { ObjectType } from '@nestjs/graphql';

/**
 * Graphql type: Additional resources of a lesson.
 */
@ObjectType()
export class LessonResources {
	links: string[];
	files: string[];
}
