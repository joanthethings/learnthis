import { ObjectType } from '@nestjs/graphql';
import { Lesson } from './lesson.gqltype';

/**
 * Graphql type: Section of a course with all its data (for admin purposes).
 */
@ObjectType()
export class Section {
	_id: string;
	title: string;
	description?: string;
	visibility: boolean;
	lessons: Lesson[];
}
