import { CourseModels } from '@Courses/enums/course-models.enum';
import { ILessonDoc } from '@Courses/interfaces/lesson-document.interface';
import { Field, Int, InterfaceType, ObjectType } from '@nestjs/graphql';

/**
 * Graphql interface type: Lesson in a course that only contains data that can be sent to the student.
 */
@InterfaceType({
	resolveType(lesson: ILessonDoc) {
		if (lesson.kind === CourseModels.LESSON_VIDEO) {
			return VideoLessonPublic;
		} else if (lesson.kind === CourseModels.LESSON_INFO) {
			return InfoLessonPublic;
		}
	},
})
export abstract class LessonPublic {
	_id: string;
	title: string;
	@Field(type => Int)
	section: number;
}

/**
 * Graphql type: Video lesson of a course that only contains data that can be sent to the student.
 */
@ObjectType({ implements: [LessonPublic] })
export class VideoLessonPublic extends LessonPublic {
	@Field(type => Int)
	duration: number;
}

/**
 * Graphql type: Info lesson of a course that only contains data that can be sent to the student.
 */
@ObjectType({ implements: [LessonPublic] })
export class InfoLessonPublic extends LessonPublic {}
