import { CommonErrors } from '@Common/enums/common-errors.enum';
import { cleanObject, trimAllStrings } from '@Common/utils/clean-object';
import { CourseModifyDto } from '@Courses/dto/course-modify.dto';
import { CourseErrors } from '@Courses/enums/course-errors.enum';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { CourseValidation } from 'learnthis-utils';

@Injectable()
export class CourseModifyPipe implements PipeTransform {
	transform(value: CourseModifyDto): CourseModifyDto {
		if (!Object.keys(value).length)
			throw new BadRequestException(CommonErrors.NOTHING_TO_MODIFY);

		const { title, description, price, compareAtPrice, url } = value;

		const errors = [];

		if (title && !CourseValidation.titleValidation(title))
			errors.push(CommonErrors.FORMAT_INVALID_TITLE);

		if (description && !CourseValidation.descriptionValidation(description))
			errors.push(CommonErrors.FORMAT_INVALID_DESCRIPTION);

		if (price && !CourseValidation.priceValidation(price))
			errors.push(CourseErrors.FORMAT_COURSE_PRICE);

		if (compareAtPrice && !CourseValidation.priceValidation(compareAtPrice))
			errors.push(CourseErrors.FORMAT_COURSE_COMPARE_AT);

		if (price && compareAtPrice && price >= compareAtPrice)
			errors.push(CourseErrors.COURSE_COMPARE_AT_LESS_THAN_PRICE);

		if (url && !CourseValidation.urlValidation(url))
			errors.push(CommonErrors.FORMAT_URL);

		if (errors.length > 0) {
			throw new BadRequestException(errors.join('. '));
		}

		cleanObject(value);
		trimAllStrings(value);

		return value;
	}
}
