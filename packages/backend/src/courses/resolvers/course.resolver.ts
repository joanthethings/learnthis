import { PaginateDto } from '@Common/dto/paginate.dto';
import { IPaginate } from '@Common/interfaces/mongoose.interface';
import { ObjectIDArrayPipe } from '@Common/pipes/objectid-array.pipe';
import { ObjectIDPipe } from '@Common/pipes/objectid.pipe';
import { ObjectID } from '@Common/types/object-id.type';
import { CourseCreateDto } from '@Courses/dto/course-create.dto';
import { CourseModifyDto } from '@Courses/dto/course-modify.dto';
import { SectionCreateDto } from '@Courses/dto/section-create.dto';
import { SectionModifyDto } from '@Courses/dto/section-modify.dto';
import { CourseErrors } from '@Courses/enums/course-errors.enum';
import { CoursePaginated } from '@Courses/gql-types/course-paginated.gqltype';
import { CourseWithSectionsPublic } from '@Courses/gql-types/course-with-sections-public.gqltype';
import { CourseWithSections } from '@Courses/gql-types/course-with-sections.gqltype';
import { Course } from '@Courses/gql-types/course.gqltype';
import { ICourseDoc } from '@Courses/interfaces/course-document.interface';
import { ILessonDoc } from '@Courses/interfaces/lesson-document.interface';
import { CourseCreatePipe } from '@Courses/pipes/course-create.pipe';
import { CourseModifyPipe } from '@Courses/pipes/course-modify.pipe';
import { SectionCreatePipe } from '@Courses/pipes/section-create.pipe';
import { SectionModifyPipe } from '@Courses/pipes/section-modify.pipe';
import { CourseService } from '@Courses/services/course.service';
import { NotFoundException, UseGuards } from '@nestjs/common';
import { Args, ID, Mutation, Query, Resolver } from '@nestjs/graphql';
import { AllowedWorkerRoles } from '@Workers/auth/allowed-worker-roles.decorator';
import { WorkerGqlAuthGuard } from '@Workers/auth/worker-gql-auth.guard';
import { WorkerGqlRolesGuard } from '@Workers/auth/worker-gql-roles.guard';
import { WorkerRoles } from '@Workers/enums/worker-roles.enum';

/**
 * Course graphql queries and resolvers.
 */
@Resolver(() => Course)
export class CourseResolver {
	/**
	 * Dependency injection.
	 * @param courseService Course service
	 */
	constructor(private readonly courseService: CourseService) {}

	//#region Public

	/**
	 * Finds all existing courses.
	 * Only returns courses whose visibility is public.
	 *
	 * - AUTH: Public
	 * @returns Courses array
	 */
	@Query(() => [Course])
	course_public_find(): Promise<ICourseDoc[]> {
		return this.courseService.publicFind();
	}

	/**
	 * Finds a course by url and populates all the lessons.
	 * Only returns courses whose visibility is public.
	 *
	 * - AUTH: Public
	 * @param courseUrl Course url
	 * @returns Course data
	 */
	@Query(() => CourseWithSectionsPublic)
	async course_public_find_by_url(
		@Args('courseUrl') courseUrl: string
	): Promise<ICourseDoc<ILessonDoc>> {
		const course = await this.courseService.publicFindByUrlAndPopulate(
			courseUrl
		);
		if (!course) throw new NotFoundException(CourseErrors.COURSE_NOT_FOUND);
		return course;
	}

	/**
	 * Finds some courses by an array of urls.
	 * Only returns courses whose visibility is public.
	 *
	 * - AUTH: Public
	 * @param coursesUrls Course urls array
	 * @returns Courses data array
	 */
	@Query(() => [Course])
	course_public_find_by_url_array(
		@Args('coursesUrls', { type: () => [String!]! })
		coursesUrls: ObjectID[]
	): Promise<ICourseDoc[]> {
		return this.courseService.publicFindByUrlArray(coursesUrls);
	}

	//#endregion

	//#region Find

	/**
	 * Finds all existing courses (with pagination).
	 *
	 * - AUTH: Workers
	 * - ROLES: All
	 * @param paginate Pagination options
	 * @returns Pagination with courses array
	 */
	@Query(() => CoursePaginated)
	@UseGuards(WorkerGqlAuthGuard)
	course_admin_find(
		@Args('paginate', { type: () => PaginateDto, nullable: true })
		paginate: PaginateDto
	): Promise<IPaginate<ICourseDoc>> {
		return this.courseService.findPaginate(
			(paginate && paginate.offset) || 0,
			(paginate && paginate.limit) || 10
		);
	}

	/**
	 *	Finds a course by id (and populate course lessons).
	 *
	 * - AUTH: Workers
	 * - ROLES: All
	 * @param courseId Course ObjectId
	 * @returns Course data
	 */
	@Query(() => CourseWithSections)
	@UseGuards(WorkerGqlAuthGuard)
	async course_admin_find_by_id(
		@Args('courseId', { type: () => ID }, ObjectIDPipe) courseId: ObjectID
	): Promise<ICourseDoc<ILessonDoc>> {
		const course = await this.courseService.findByIdAndPopulate(courseId);
		if (!course) throw new NotFoundException(CourseErrors.COURSE_NOT_FOUND);
		return course;
	}

	/**
	 * Finds some courses by an array of ids.
	 *
	 * - AUTH: Workers
	 * - ROLES: All
	 * @param courseIds Course ObjectIds array
	 * @returns Courses array
	 */
	@Query(() => [Course])
	@UseGuards(WorkerGqlAuthGuard)
	course_admin_find_by_id_array(
		@Args('courseIds', { type: () => [ID], nullable: true }, ObjectIDArrayPipe)
		courseIds: ObjectID[]
	): Promise<ICourseDoc[]> {
		return this.courseService.findByIdArray(courseIds);
	}

	//#endregion

	//#region Courses

	/**
	 * Creates a new course.
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param courseData Course creation data
	 * @returns Course object id
	 */
	@Mutation(() => ID)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async course_admin_create(
		@Args('courseData', { type: () => CourseCreateDto }, CourseCreatePipe)
		courseData: CourseCreateDto
	): Promise<ObjectID> {
		return this.courseService.create(courseData);
	}

	/**
	 * Modifies an axisting course data.
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param courseId Course ObjectId
	 * @param input New course data
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async course_admin_modify(
		@Args('courseId', { type: () => ID }, ObjectIDPipe)
		courseId: ObjectID,
		@Args('input', { type: () => CourseModifyDto }, CourseModifyPipe)
		input: CourseModifyDto
	): Promise<boolean> {
		await this.courseService.modifyCourse(courseId, input);
		return true;
	}

	/**
	 * Sorts sections in a course.
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param courseId Course ObjectId
	 * @param sectionIdsSorted Section ObjectIds array
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async course_admin_sort_sections(
		@Args('courseId', { type: () => ID }, ObjectIDPipe)
		courseId: ObjectID,
		@Args('sectionIdsSorted', { type: () => [ID] }, ObjectIDArrayPipe)
		sectionIdsSorted: ObjectID[]
	): Promise<boolean> {
		await this.courseService.sortSections(courseId, sectionIdsSorted);
		return true;
	}

	/**
	 * Removes an existing course.
	 * Before deleting a course, it is necessary to have previously deleted all its sections.
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param courseId Course ObjectId
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async course_admin_delete(
		@Args('courseId', { type: () => ID }, ObjectIDPipe)
		courseId: ObjectID
	): Promise<boolean> {
		await this.courseService.deleteCourse(courseId);
		return true;
	}

	//#endregion

	//#region Sections

	/**
	 * Create a new section within a course.
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param courseId Course ObjectId
	 * @param input Section data
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async course_admin_section_create(
		@Args('courseId', { type: () => ID }, ObjectIDPipe)
		courseId: ObjectID,
		@Args('input', { type: () => SectionCreateDto }, SectionCreatePipe)
		input: SectionCreateDto
	): Promise<boolean> {
		await this.courseService.createSection(courseId, input);
		return true;
	}

	/**
	 * Modifies section data.
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param courseId Course ObjectId
	 * @param sectionId Section ObjectId
	 * @param input New section data
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async course_admin_section_modify(
		@Args('courseId', { type: () => ID }, ObjectIDPipe)
		courseId: ObjectID,
		@Args('sectionId', { type: () => ID }, ObjectIDPipe)
		sectionId: ObjectID,
		@Args('input', { type: () => SectionModifyDto }, SectionModifyPipe)
		input: SectionModifyDto
	): Promise<boolean> {
		await this.courseService.modifySection(courseId, sectionId, input);
		return true;
	}

	/**
	 * Sorts lessons in a course.
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param courseId Course ObjectId
	 * @param sectionId Section ObjectId
	 * @param lessonIdsSorted Lesson ObjectIds array
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async course_admin_section_sort_lessons(
		@Args('courseId', { type: () => ID }, ObjectIDPipe)
		courseId: ObjectID,
		@Args('sectionId', { type: () => ID }, ObjectIDPipe)
		sectionId: ObjectID,
		@Args('lessonIdsSorted', { type: () => [ID] }, ObjectIDArrayPipe)
		lessonIdsSorted: ObjectID[]
	): Promise<boolean> {
		await this.courseService.sortLessons(courseId, sectionId, lessonIdsSorted);
		return true;
	}

	/**
	 * Deletes a section from a course.
	 * Before deleting a section, it is necessary to have previously deleted all its lessons.
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param courseId Course ObjectId
	 * @param sectionId Section ObjectId
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async course_admin_section_delete(
		@Args('courseId', { type: () => ID }, ObjectIDPipe)
		courseId: ObjectID,
		@Args('sectionId', { type: () => ID }, ObjectIDPipe)
		sectionId: ObjectID
	): Promise<boolean> {
		await this.courseService.deleteSection(courseId, sectionId);
		return true;
	}

	//#endregion

	//#region Publish

	/**
	 * Publishes a course.
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param courseId Course ObjectId
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async course_admin_publish(
		@Args('courseId', { type: () => ID }, ObjectIDPipe) courseId: ObjectID
	): Promise<boolean> {
		await this.courseService.publish(courseId);
		return true;
	}

	/**
	 * Unpublishes a course.
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param courseId Course ObjectId
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async course_admin_unpublish(
		@Args('courseId', { type: () => ID }, ObjectIDPipe) courseId: ObjectID
	): Promise<boolean> {
		await this.courseService.unpublish(courseId);
		return true;
	}

	/**
	 * Publishes a section.
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param courseId Course ObjectId
	 * @param sectionId Section ObjectId
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async course_admin_section_publish(
		@Args('courseId', { type: () => ID }, ObjectIDPipe)
		courseId: ObjectID,
		@Args('sectionId', { type: () => ID }, ObjectIDPipe)
		sectionId: ObjectID
	) {
		await this.courseService.publishSection(courseId, sectionId);
		return true;
	}

	/**
	 * Unpublishes a section.
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param courseId Course ObjectId
	 * @param sectionId Section ObjectId
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async course_admin_section_unpublish(
		@Args('courseId', { type: () => ID }, ObjectIDPipe)
		courseId: ObjectID,
		@Args('sectionId', { type: () => ID }, ObjectIDPipe)
		sectionId: ObjectID
	) {
		await this.courseService.unpublishSection(courseId, sectionId);
		return true;
	}

	//#endregion

	//#region Workers

	/**
	 * Assing a worker to a course and vice versa
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param courseId Course ObjectId
	 * @param workerId Worker ObjectId
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async course_asign_worker(
		@Args('courseId', { type: () => ID }, ObjectIDPipe) courseId: ObjectID,
		@Args('workerId', { type: () => ID }, ObjectIDPipe) workerId: ObjectID
	): Promise<boolean> {
		await this.courseService.asignWorkerToCourse(courseId, workerId);
		return true;
	}

	/**
	 * Unassing a worker from a course and vice versa
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param courseId Course ObjectId
	 * @param workerId Worker ObjectId
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async course_unasign_worker(
		@Args('courseId', { type: () => ID }, ObjectIDPipe) courseId: ObjectID,
		@Args('workerId', { type: () => ID }, ObjectIDPipe) workerId: ObjectID
	): Promise<boolean> {
		await this.courseService.unassignWorkerFromCourse(courseId, workerId);
		return true;
	}

	//#endregion
}
