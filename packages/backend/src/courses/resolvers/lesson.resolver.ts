import { Args, ID, Int, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Lesson } from '@Courses/gql-types/lesson.gqltype';
import { LessonService } from '@Courses/services/lesson.service';
import { LessonCreateDto } from '@Courses/dto/lesson-create.dto';
import { AllowedWorkerRoles } from '@Workers/auth/allowed-worker-roles.decorator';
import { UseGuards } from '@nestjs/common';
import { WorkerGqlAuthGuard } from '@Workers/auth/worker-gql-auth.guard';
import { WorkerRoles } from '@Workers/enums/worker-roles.enum';
import { WorkerGqlRolesGuard } from '@Workers/auth/worker-gql-roles.guard';
import { ObjectIDPipe } from '@Common/pipes/objectid.pipe';
import { ObjectID } from '@Common/types/object-id.type';
import { ArrayIndexPipe } from '@Common/pipes/array-index.pipe';
import { LessonCreatePipe } from '@Courses/pipes/lesson-create.pipe';
import { LessonResourcesDto } from '@Courses/dto/lesson-resources.dto';
import { LessonResourcesPipe } from '@Courses/pipes/lesson-resources.pipe';
import { ILessonDoc } from '@Courses/interfaces/lesson-document.interface';

/**
 * Lesson graphql queries and resolvers.
 */
@Resolver(() => Lesson)
export class LessonResolver {
	/**
	 * Dependency injection.
	 * @param lessonService Lesson service
	 */
	constructor(private readonly lessonService: LessonService) {}

	//#region Public

	/**
	 * Finds a lesson by id.
	 *
	 * - AUTH: Public
	 * @param {ObjectID} lessonId Lesson ObjectId
	 * @returns Lesson data
	 */
	@Query(() => Lesson)
	async lesson_public_find_by_id(
		@Args('id', { type: () => ID }, ObjectIDPipe) lessonId: ObjectID
	): Promise<ILessonDoc | undefined> {
		return await this.lessonService.publicFindById(lessonId);
	}

	//#endregion

	//#region Find

	/**
	 * Finds a lesson by id.
	 *
	 * - AUTH: Workers
	 * - ROLES: All
	 * @param {ObjectID} lessonId Lesson ObjectId
	 * @returns Lesson data
	 */
	@Query(() => Lesson)
	@UseGuards(WorkerGqlAuthGuard)
	async lesson_admin_find_by_id(
		@Args('id', { type: () => ID }, ObjectIDPipe) lessonId: ObjectID
	) {
		return await this.lessonService.findById(lessonId);
	}

	//#endregion

	//#region Lesson

	/**
	 * Creates a new video or info lesson, and adds it to a course section.
	 *
	 * - AUTH: Workers
	 * - ROLES: All
	 * @param {ObjectID} courseId Course ObjectId
	 * @param {number} sectionIndex Section index on course sections array
	 * @param {LessonCreateDto} input Lesson creation data
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	@UseGuards(WorkerGqlAuthGuard)
	async lesson_admin_create(
		@Args('courseId', ObjectIDPipe)
		courseId: ObjectID,
		@Args('sectionIndex', { type: () => Int }, ArrayIndexPipe)
		sectionIndex: number,
		@Args('input', { type: () => LessonCreateDto }, LessonCreatePipe)
		input: LessonCreateDto
	) {
		await this.lessonService.createLesson(courseId, sectionIndex, input);
		return true;
	}

	/**
	 * Updates lesson resources.
	 *
	 * - AUTH: Workers
	 * - ROLES: All
	 * @param {ObjectID} lessonId Course ObjectId
	 * @param {LessonResourcesDto} input Resources data
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	@UseGuards(WorkerGqlAuthGuard)
	async lesson_admin_update_resources(
		@Args('lessonId', ObjectIDPipe)
		lessonId: ObjectID,
		@Args('input', { type: () => LessonResourcesDto }, LessonResourcesPipe)
		input: LessonResourcesDto
	) {
		await this.lessonService.updateResources(lessonId, input);
		return true;
	}

	/**
	 * Deletes a lesson by id.
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param {ObjectID} lessonId Lesson ObjectId
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async lesson_admin_delete(
		@Args('lessonId', ObjectIDPipe) lessonId: ObjectID
	) {
		await this.lessonService.deleteById(lessonId);
		return true;
	}

	//#endregion

	//#region Publish

	/**
	 * Publishes a lesson.
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param {ObjectID} lessonId Lesson ObjectId
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async lesson_admin_publish(
		@Args('lessonId', ObjectIDPipe) lessonId: ObjectID
	) {
		await this.lessonService.publish(lessonId);
		return true;
	}

	/**
	 * Unpublishes a lesson.
	 *
	 * - AUTH: Workers
	 * - ROLES: Admin
	 * @param {ObjectID} lessonId Lesson ObjectId
	 * @returns True if success
	 */
	@Mutation(() => Boolean)
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerGqlAuthGuard, WorkerGqlRolesGuard)
	async lesson_admin_unpublish(
		@Args('lessonId', ObjectIDPipe) lessonId: ObjectID
	) {
		await this.lessonService.unpublish(lessonId);
		return true;
	}

	//#endregion
}
