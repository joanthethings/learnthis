import { CourseModels } from '@Courses/enums/course-models.enum';
import { Course } from '@Courses/schemas/course.schema';
import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Schema as MongooseSchema } from 'mongoose';

type ResourcesProps = 'links' | 'files';

interface IResourcesProps {
	links: string[];
	files: string[];
}

@Schema()
export class VideoLesson {
	@Prop({ type: String })
	videoSrc: string;

	@Prop({ type: Number })
	duration: number;
}

@Schema()
export class InfoLesson {
	@Prop({ type: String })
	content: string;
}

@Schema({ discriminatorKey: 'kind' })
export class Lesson {
	@Prop({ type: String, required: true })
	title: string;

	@Prop({ type: String })
	description: string;

	@Prop({ type: MongooseSchema.Types.ObjectId, ref: CourseModels.COURSE })
	course: Course;

	@Prop({ type: Boolean, required: true, default: false })
	visibility: boolean;

	@Prop(
		raw({
			links: [{ type: String }],
			files: [{ type: String }],
		})
	)
	resources: Record<ResourcesProps, IResourcesProps>;

	@Prop({
		type: String,
		required: true,
		enum: [CourseModels.LESSON_VIDEO, CourseModels.LESSON_INFO],
	})
	kind: string;
}

export const LessonSchema = SchemaFactory.createForClass(Lesson);
export const VideoLessonSchema = SchemaFactory.createForClass(VideoLesson);
export const InfoLessonSchema = SchemaFactory.createForClass(InfoLesson);
