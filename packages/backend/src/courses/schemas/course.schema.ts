import { CourseModels } from '@Courses/enums/course-models.enum';
import { Lesson } from '@Courses/schemas/lesson.schema';
import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { StudentModels } from '@Students/enums/student-models.enum';
import { Student } from '@Students/schemas/students.schema';
import { WorkerModels } from '@Workers/enums/worker-models.enum';
import { Worker } from '@Workers/schemas/worker.schema';
import { Schema as MongooseSchema } from 'mongoose';

type SectionProps = 'title' | 'description' | 'visibility' | 'lessons';

interface ISectionProps {
	title: string;
	description: string;
	visibility: boolean;
	lessons: any[];
}

@Schema()
export class Course {
	@Prop({ type: String, required: true, unique: true })
	url: string;

	@Prop({ type: String, required: true })
	title: string;

	@Prop({ type: String })
	description: string;

	@Prop({ type: String })
	image: string;

	@Prop({ type: String })
	background: string;

	@Prop({ type: Number, required: true })
	price: number;

	@Prop({ type: Number })
	compareAtPrice: number;

	@Prop({ type: Boolean, required: true, default: false })
	visibility: boolean;

	@Prop({
		type: [{ type: MongooseSchema.Types.ObjectId, ref: StudentModels.STUDENT }],
	})
	studentsEnrolled: Student[];

	@Prop({
		type: [{ type: MongooseSchema.Types.ObjectId, ref: WorkerModels.WORKER }],
	})
	tutors: Worker[];

	@Prop(
		raw([
			{
				title: { type: String, required: true },
				description: { type: String },
				visibility: { type: Boolean, required: true },
				lessons: [
					{ type: MongooseSchema.Types.ObjectId, ref: CourseModels.LESSON },
				],
			},
		])
	)
	sections: Record<SectionProps, ISectionProps>[];
}

export const CourseSchema = SchemaFactory.createForClass(Course);

CourseSchema.virtual('studentsCount').get(function () {
	if (!this.studentsEnrolled) return 0;
	return this.studentsEnrolled.length;
});
