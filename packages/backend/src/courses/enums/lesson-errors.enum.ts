export enum LessonErrors {
	LESSON_NOT_FOUND = 'Lección no encontrada',
	LESSON_ALREADY_PUBLISHED = 'La lección ya es pública',
	LESSON_ALREADY_PRIVATE = 'La lección ya es privada',
	VIDEOLESSON_NOT_VIDEO = 'La lección no tiene video',
}
