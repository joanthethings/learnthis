import { registerEnumType } from '@nestjs/graphql';

export enum LessonTypes {
	VIDEO = 'VideoLesson',
	INFO = 'InfoLesson',
}

registerEnumType(LessonTypes, { name: 'LessonTypes' });
