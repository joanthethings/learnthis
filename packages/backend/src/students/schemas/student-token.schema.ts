import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { StudentModels } from '@Students/enums/student-models.enum';
import { StudentTokenTypes } from '@Students/enums/student-token-types.enum';
import { Schema as MongooseSchema } from 'mongoose';
import { Student } from './students.schema';

/**
 * Mongoose student token's schema definition
 */
@Schema()
export class StudentToken {
	/** JWT Token */
	@Prop({ type: String, required: true, unique: true, index: true })
	token: string;

	/** Token owner student */
	@Prop({ type: MongooseSchema.Types.ObjectId, ref: StudentModels.STUDENT })
	student: Student;

	/** Token's type */
	@Prop({ type: String, enum: Object.values(StudentTokenTypes) })
	type: StudentTokenTypes;

	/** Token's expiration date */
	@Prop({
		type: Date,
		required: true,
		index: { expires: 1, sparse: true },
	})
	expiresAt: Date;
}

/**
 * Mongoose student token's schema object
 */
export const StudentTokenSchema = SchemaFactory.createForClass(StudentToken);
