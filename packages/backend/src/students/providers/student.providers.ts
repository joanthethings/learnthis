import { Provider } from '@nestjs/common';
import { StudentsResolver } from '@Students/resolvers/students.resolver';
import { StudentsService } from '@Students/services/students.service';

/**
 * Student providers
 */
export const studentProviders: Provider[] = [StudentsService, StudentsResolver];
