import { Gender } from '@Common/enums/gender.enum';
import { ObjectID } from '@Common/types/object-id.type';
import { ICourseDoc } from '@Courses/interfaces/course-document.interface';
import { ILessonDoc } from '@Courses/interfaces/lesson-document.interface';
import { IOrderDoc } from '@Orders/interfaces/order-document.interface';
import { SocialType } from '@Students/enums/social-type.enum';
import { Document } from 'mongoose';

export type OrdersOrObjectId = IOrderDoc | ObjectID;
export type CartOrObjectId = ICourseDoc | ObjectID;
export type LessonsProgressOrObjectID = ILessonDoc | ObjectID;
export type CoursesEnrolledOrObjectID = ICourseDoc | ObjectID;

export type IStudentModel = IStudentDoc<
	CartOrObjectId,
	CoursesEnrolledOrObjectID,
	LessonsProgressOrObjectID,
	OrdersOrObjectId
>;

export interface IStudent<
	R extends CartOrObjectId = ObjectID,
	S extends CoursesEnrolledOrObjectID = ObjectID,
	T extends LessonsProgressOrObjectID = ObjectID,
	U extends OrdersOrObjectId = ObjectID
> {
	email: string;
	name: string;
	surname: string;
	active: boolean;
	password?: string;
	username?: string;
	photo?: string;
	bio?: string;
	gender?: Gender;
	birthDate?: Date;
	socialAccounts: ISocialAccount[];
	cart: R[];
	coursesEnrolled: ICourseEnrolled<S, T>[];
	orders: U[];
}

export interface ISocialAccount {
	id: string;
	type: SocialType;
}

export interface ICourseEnrolled<
	S extends CoursesEnrolledOrObjectID = ObjectID,
	T extends LessonsProgressOrObjectID = ObjectID
> {
	course: S;
	progress: ICourseProgress<T>;
}

export interface ICourseProgress<
	T extends LessonsProgressOrObjectID = ObjectID
> {
	currentLesson: T;
	lessons: {
		lesson: T;
		completed: boolean;
	}[];
}

export interface IStudentDoc<
	R extends CartOrObjectId = ObjectID,
	S extends CoursesEnrolledOrObjectID = ObjectID,
	T extends LessonsProgressOrObjectID = ObjectID,
	U extends OrdersOrObjectId = ObjectID
> extends IStudent<R, S, T, U>,
		Document {}
