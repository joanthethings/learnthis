import { BaseJwtAuthGuard } from '@Common/auth/base-jwt-auth.guard';
import { CommonErrors } from '@Common/enums/common-errors.enum';
import { Env } from '@Common/enums/env.enum';
import {
	ExecutionContext,
	Injectable,
	UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { StudentsService } from '@Students/services/students.service';

/**
 * Base jwt auth guard to get student from the context.
 *
 * - Its necessary to implement the method to get the request from the context.
 */
@Injectable()
export abstract class StudentBaseAuthGuard extends BaseJwtAuthGuard {
	/**
	 * Dependency injection
	 * @param jwtservice JWt service
	 * @param studentsService Students service
	 * @param configService Config service
	 */
	constructor(
		protected readonly jwtservice: JwtService,
		private readonly studentsService: StudentsService,
		private readonly configService: ConfigService
	) {
		super(jwtservice);
	}

	/**
	 * Get HTTP request from context.
	 * @param context Execution context
	 */
	abstract getRequest(context: ExecutionContext): any;

	/** Gets JWT secret */
	getSecret() {
		return this.configService.get(Env.STUDENT_TOKEN_KEY);
	}

	/**
	 * Sets user to request.user
	 * @param userId User's ObjectId
	 * @param req HTTP request
	 */
	async setUserToRequest(userId: string, req: any) {
		const student = await this.studentsService.findById(userId);

		if (!student || !student.active)
			throw new UnauthorizedException(CommonErrors.UNAUTHORIZED);
		req.user = student;
	}
}
