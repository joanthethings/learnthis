import { ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { StudentBaseAuthGuard } from './student-base-auth.guard';

/**
 * Only for GRAPHQL requests.
 * Extracts student from the context and sets it to req.user.
 */
export class StudentGqlAuthGuard extends StudentBaseAuthGuard {
	/**
	 * Get HTTP request from context.
	 * @param context Execution context
	 */
	getRequest(context: ExecutionContext) {
		return GqlExecutionContext.create(context).getContext().req;
	}
}
