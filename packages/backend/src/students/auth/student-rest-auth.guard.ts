import { ExecutionContext, Injectable } from '@nestjs/common';
import { StudentBaseAuthGuard } from './student-base-auth.guard';

/**
 * Only for REST requests.
 * Extracts student from the context and sets it to req.user.
 */
@Injectable()
export class StudentRestAuthGuard extends StudentBaseAuthGuard {
	/**
	 * Extracts student from JWT token and adds it to req.user
	 * @param context Execution context
	 */
	getRequest(context: ExecutionContext) {
		return context.switchToHttp().getRequest();
	}
}
