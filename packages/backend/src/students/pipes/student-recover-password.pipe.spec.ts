import { BadRequestException } from '@nestjs/common';
import { StudentRecoverPasswordPipe } from './student-recover-password.pipe';
import { StudentRecoverPasswordDto } from '@Students/dto/student-recover-password.dto';

describe('RecoverPasswordPipe', () => {
	let recoverPasswordPipe: StudentRecoverPasswordPipe;

	beforeAll(() => (recoverPasswordPipe = new StudentRecoverPasswordPipe()));

	it('Todo correcto', () => {
		const recoverPassword: StudentRecoverPasswordDto = {
			newPassword: 'asd123ASD.',
			token:
				'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c',
		};
		expect(recoverPasswordPipe.transform(recoverPassword)).toBe(
			recoverPassword
		);
	});

	it('Contraseña incorrecta', () => {
		const recoverPassword: StudentRecoverPasswordDto = {
			newPassword: 'ásd123',
			token:
				'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c',
		};
		expect(() => recoverPasswordPipe.transform(recoverPassword)).toThrow(
			BadRequestException
		);
	});

	it('Token incorrecto', () => {
		const recoverPassword: StudentRecoverPasswordDto = {
			newPassword: 'asd123ASD.',
			token:
				'eyJhbR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c',
		};
		expect(() => recoverPasswordPipe.transform(recoverPassword)).toThrow(
			BadRequestException
		);
	});
});
