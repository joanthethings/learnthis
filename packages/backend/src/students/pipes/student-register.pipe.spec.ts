import { BadRequestException } from '@nestjs/common';
import { StudentRegisterDto } from '@Students/dto/student-register.dto';
import { StudentRegisterPipe } from './student-register.pipe';

describe('RegisterPipe', () => {
	let registerPipe: StudentRegisterPipe;

	beforeAll(() => (registerPipe = new StudentRegisterPipe()));

	it('Todo correcto', () => {
		const register: StudentRegisterDto = {
			email: 'yoquese@gmail.com',
			name: 'Albert',
			surname: 'hvdev',
			password: 'asd123ASD.',
		};
		expect(registerPipe.transform(register)).toBe(register);
	});

	it('Email incorrecto', () => {
		const register: StudentRegisterDto = {
			email: 'yoquese@gmail.com.es.',
			name: 'Albert',
			surname: 'hvdev',
			password: 'asd123ASD.',
		};
		expect(() => registerPipe.transform(register)).toThrow(BadRequestException);
	});

	it('Nombre incorrecto', () => {
		const register: StudentRegisterDto = {
			email: 'yoquese@gmail.com',
			name: 'Albert12',
			surname: 'hvdev',
			password: 'asd123ASD.',
		};
		expect(() => registerPipe.transform(register)).toThrow(BadRequestException);
	});

	it('Apellido incorrecto', () => {
		const register: StudentRegisterDto = {
			email: 'yoquese@gmail.com',
			name: 'Albert',
			surname: 'Crack-32',
			password: 'asd123ASD.',
		};
		expect(() => registerPipe.transform(register)).toThrow(BadRequestException);
	});

	it('Contraseña incorrecta', () => {
		const register: StudentRegisterDto = {
			email: 'yoquese@gmail.com',
			name: 'Albert',
			surname: 'Crack',
			password: 'ásd123äSD.',
		};
		expect(() => registerPipe.transform(register)).toThrow(BadRequestException);
	});
});
