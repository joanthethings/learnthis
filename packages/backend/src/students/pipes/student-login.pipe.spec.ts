import { BadRequestException } from '@nestjs/common';
import { StudentLoginDto } from '@Students/dto/student-login.dto';
import { StudentLoginPipe } from './student-login.pipe';

describe('StudentLoginPipe', () => {
	let loginPipe: StudentLoginPipe;

	beforeAll(() => (loginPipe = new StudentLoginPipe()));

	it('Email y contraseña correctos', () => {
		const login: StudentLoginDto = {
			email: 'algo@algo.com',
			password: 'asd123ASD.',
		};
		expect(loginPipe.transform(login)).toBe(login);
	});

	it('Email incorrecto y contraseña correcta', () => {
		const login: StudentLoginDto = {
			email: 'algo@algo.com.',
			password: 'asd123ASD.',
		};
		expect(() => loginPipe.transform(login)).toThrow(BadRequestException);
	});

	it('Email correcto y contraseña incorrecta', () => {
		const login: StudentLoginDto = {
			email: 'algo@algo.com',
			password: 'asd12',
		};
		expect(() => loginPipe.transform(login)).toThrow(BadRequestException);
	});

	it('Email incorrecto y contraseña incorrecta', () => {
		const login: StudentLoginDto = {
			email: 'algo@algo.com.',
			password: 'asd12',
		};
		expect(() => loginPipe.transform(login)).toThrow(BadRequestException);
	});
});
