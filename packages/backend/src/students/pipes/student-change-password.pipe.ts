import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { StudentChangePasswordDto } from '@Students/dto/student-change-password.dto';
import { StudentErrors } from '@Students/enums/student-errors.enum';
import { FormValidation } from 'learnthis-utils';

/**
 * StudentChangePasswordDto format validation.
 */
@Injectable()
export class StudentChangePasswordPipe implements PipeTransform {
	/**
	 * Transform pipe.
	 * - Trims new password
	 *
	 * @param value Dto
	 * @returns Trimmed dto
	 */
	transform(value: StudentChangePasswordDto) {
		if (value.oldPassword === value.newPassword)
			throw new BadRequestException(StudentErrors.FORMAT_SAME_PASSWORD);

		if (
			value.oldPassword &&
			!FormValidation.passwordValidation(value.oldPassword)
		)
			throw new BadRequestException(StudentErrors.FORMAT_PASSWORD);
		if (!FormValidation.passwordValidation(value.newPassword))
			throw new BadRequestException(StudentErrors.FORMAT_PASSWORD);

		value.newPassword = value.newPassword.trim();

		return value;
	}
}
