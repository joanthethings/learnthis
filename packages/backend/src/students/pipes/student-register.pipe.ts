import { StudentErrors } from '@Students/enums/student-errors.enum';
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { FormValidation } from 'learnthis-utils';
import { StudentRegisterDto } from '../dto/student-register.dto';
import { trimAllStrings } from '@Common/utils/clean-object';

/**
 * StudentRegisterDto format validation and transform.
 */
@Injectable()
export class StudentRegisterPipe implements PipeTransform {
	/**
	 * Transform pipe.
	 * @param value Dto
	 * @returns Dto trimmed
	 */
	transform(value: StudentRegisterDto): StudentRegisterDto {
		const { email, name, surname, password } = value;

		const errors = [];

		if (!FormValidation.emailValidation(email))
			errors.push(StudentErrors.FORMAT_EMAIL);

		if (!FormValidation.nameValidation(name))
			errors.push(StudentErrors.FORMAT_NAME);

		if (!FormValidation.nameValidation(surname))
			errors.push(StudentErrors.FORMAT_SURNAME);

		if (!FormValidation.passwordValidation(password))
			errors.push(StudentErrors.FORMAT_PASSWORD);

		if (errors.length > 0) {
			throw new BadRequestException(errors.join('. '));
		}

		trimAllStrings(value);

		return value;
	}
}
