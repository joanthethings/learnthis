import { JwtToken } from '@Common/types/jwt-token.type';
import { Field, ID, ObjectType } from '@nestjs/graphql';
import { StudentProfile } from './student-profile.gqltype';

/**
 * Graphql type: Student's login response
 */
@ObjectType()
export class StudentLogin {
	/** JWT token */
	@Field(() => ID)
	token: JwtToken;
	/** Student's data */
	user: StudentProfile;
}
