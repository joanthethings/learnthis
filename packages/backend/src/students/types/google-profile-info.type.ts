/**
 * Response type for https://www.googleapis.com/oauth2/v2/userinfo
 */
export type GoogleProfileInfo = {
	/** The users first name */
	given_name: string;
	/** The user's last name */
	family_name: string;
	/** The user's full name */
	name: string;
	/** URL of the user's picture image */
	picture: string;
	/** The user's email address */
	email: string;
	/** True if email is verified */
	verified_email: boolean;
	/** The user's preferred locale */
	locale: string;
	/** The user's ID */
	id: string;
};
