import {
	ISocialAccount,
	IStudent,
} from '@Students/interfaces/student-document.interface';

/**
 * Social info parsed from oauth endpoints
 */
export type SocialAccountInfo = {
	/** Social account id and identity provider */
	socialAccount: ISocialAccount;
	/** Student data formatted following Mongoose schema */
	studentData: IStudent;
};
