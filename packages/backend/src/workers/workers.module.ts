import { AgendaModule } from '@Common/modules/agenda/agenda.module';
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';
import { WorkerGqlAuthGuard } from './auth/worker-gql-auth.guard';
import { WorkerGqlRolesGuard } from './auth/worker-gql-roles.guard';
import { WorkerController } from './controllers/worker.controller';
import { WorkerModels } from './enums/worker-models.enum';
import { workerProviders } from './providers/worker.providers';
import { WorkerSchema } from './schemas/worker.schema';
import { WorkerService } from './services/worker.service';

@Module({
	imports: [
		JwtModule.register({}),
		MongooseModule.forFeature([
			{ name: WorkerModels.WORKER, schema: WorkerSchema },
		]),
		AgendaModule,
	],
	providers: [...workerProviders, WorkerGqlAuthGuard, WorkerGqlRolesGuard],
	controllers: [WorkerController],
	exports: [WorkerService, WorkerGqlAuthGuard, WorkerGqlRolesGuard, JwtModule],
})
export class WorkersModule {}
