import { IWorkerDoc } from './worker-document.interface';

export interface WorkerLoginOutput {
	token: string;
	user: IWorkerDoc;
}
