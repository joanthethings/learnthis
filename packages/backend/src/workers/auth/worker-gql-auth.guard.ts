import { ExecutionContext, Injectable } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { WorkerBaseAuthGuard } from './worker-base-auth.guard';

/**
 * Only for GRAPHQL requests.
 * Extracts worker from the context and sets it to req.user.
 */
@Injectable()
export class WorkerGqlAuthGuard extends WorkerBaseAuthGuard {
	/**
	 * Get HTTP request from context.
	 * @param context Execution context
	 */
	getRequest(context: ExecutionContext) {
		return GqlExecutionContext.create(context).getContext().req;
	}
}
