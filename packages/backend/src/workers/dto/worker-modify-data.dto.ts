import { Field, InputType } from '@nestjs/graphql';

/**
 * Graphql input type: Modify worker data
 */
@InputType()
export class WorkerModifyDataDto {
	/** Worker's full name to display */
	@Field({ nullable: true })
	displayName?: string;
	/** Worker's email */
	@Field({ nullable: true })
	email?: string;
	/** Worker's name */
	@Field({ nullable: true })
	name?: string;
	/** Worker's surname */
	@Field({ nullable: true })
	surname?: string;
	/** Worker's username */
	@Field({ nullable: true })
	username?: string;
	/** Worker's biography */
	@Field({ nullable: true })
	bio?: string;
	/** Worker's contact phone number */
	@Field({ nullable: true })
	phone?: string;
}
