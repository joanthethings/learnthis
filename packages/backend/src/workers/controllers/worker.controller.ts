import { GetRestAuthUser } from '@Common/auth/get-user.decorator';
import { ObjectID } from '@Common/types/object-id.type';
import { ObjectIDPipe } from '@Common/pipes/objectid.pipe';
import {
	fileFilter,
	FOLDER_UPLOADS,
	imageWorker,
} from '@Common/utils/file-upload';
import {
	BadRequestException,
	Controller,
	Param,
	Post,
	UploadedFile,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { AllowedWorkerRoles } from '@Workers/auth/allowed-worker-roles.decorator';
import { WorkerRestAuthGuard } from '@Workers/auth/worker-rest-auth.guard';
import { WorkerRestRolesGuard } from '@Workers/auth/worker-rest-roles.guard';
import { WorkerRoles } from '@Workers/enums/worker-roles.enum';
import { IWorkerDoc } from '@Workers/interfaces/worker-document.interface';
import { WorkerService } from '@Workers/services/worker.service';
import { join } from 'path';
import { AgendaService } from '@Common/modules/agenda/services/agenda.service';

@Controller('worker')
export class WorkerController {
	constructor(
		private readonly workerService: WorkerService,
		private readonly agendaService: AgendaService
	) {}

	@Post('/:id/admin/image')
	@AllowedWorkerRoles(WorkerRoles.ADMIN)
	@UseGuards(WorkerRestAuthGuard, WorkerRestRolesGuard)
	@UseInterceptors(
		FileInterceptor('image', {
			fileFilter,
			storage: imageWorker,
			limits: { fileSize: 4 * 1024 * 1024 },
		})
	)
	async uploadPhotoAdmin(
		@UploadedFile() file,
		@Param('id', ObjectIDPipe) workerId: ObjectID
	): Promise<{ url: string }> {
		if (!file) throw new BadRequestException();
		this.agendaService.enQueueUploadImageProcess(
			join(FOLDER_UPLOADS, file.filename)
		);
		return {
			url: await this.workerService.setWorkerImageById(workerId, file.filename),
		};
	}

	@Post('/image')
	@UseGuards(WorkerRestAuthGuard)
	@UseInterceptors(
		FileInterceptor('image', {
			fileFilter,
			storage: imageWorker,
			limits: { fileSize: 4 * 1024 * 1024 },
		})
	)
	async uploadSelfPhoto(
		@UploadedFile() file,
		@GetRestAuthUser() worker: IWorkerDoc
	): Promise<{ url: string }> {
		if (!file) throw new BadRequestException();
		this.agendaService.enQueueUploadImageProcess(
			join(FOLDER_UPLOADS, file.filename)
		);
		return {
			url: await this.workerService.setWorkertImage(worker, file.filename),
		};
	}
}
