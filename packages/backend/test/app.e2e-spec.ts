import { CommonErrors } from '@Common/enums/common-errors.enum';
import { Gender } from '@Common/enums/gender.enum';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { StudentErrors } from '@Students/enums/student-errors.enum';
import { TokenDocument } from '@Students/interfaces/token-document.interface';
import { StudentTokenSchema } from '@Students/schemas/student-token.schema';
import { gql } from 'graphql-request';
import { MongoMemoryReplSet } from 'mongodb-memory-server';
import { connect, disconnect, model, Model } from 'mongoose';
import { join } from 'path';
import request from 'supertest';
import { AppModule } from './../src/app.module';

const sleep = time => new Promise((res, _rej) => setTimeout(res, time));

const replSet = new MongoMemoryReplSet({
	replSet: { storageEngine: 'wiredTiger', name: 'pLearnThis' },
	instanceOpts: [
		{
			port: 27018,
		},
	],
});

let authToken;

describe('Student (e2e)', () => {
	let app: INestApplication;

	beforeAll(async () => {
		//Setting up Database
		await replSet.waitUntilRunning();
		const uri = await replSet.getUri();
		connect(uri, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
			useFindAndModify: false,
			useCreateIndex: true,
		});
		process.env.DATABASE_URI = uri;

		//Starting app
		const moduleFixture: TestingModule = await Test.createTestingModule({
			imports: [AppModule],
		}).compile();

		app = moduleFixture.createNestApplication();
		await app.init();
		await sleep(1000);
	});

	it('Student - Register', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					mutation {
						student_register(
							input: {
								email: "yoquese@gmail.com"
								name: "Albert"
								surname: "hvdev"
								password: "asd123ASD."
							}
						)
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.data.student_register).toBe(true);
			});
	});

	it('Student - Register - Bad email', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					mutation {
						student_register(
							input: {
								email: "yoquese@gmail"
								name: "Albert"
								surname: "hvdev"
								password: "asd123ASD."
							}
						)
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(StudentErrors.FORMAT_EMAIL);
			});
	});

	it('Student - Register - Bad name', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					mutation {
						student_register(
							input: {
								email: "yoquese@gmail.com"
								name: "A"
								surname: "hvdev"
								password: "asd123ASD."
							}
						)
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(StudentErrors.FORMAT_NAME);
			});
	});

	it('Student - Register - Bad surname', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					mutation {
						student_register(
							input: {
								email: "yoquese@gmail.com"
								name: "Albert"
								surname: "5"
								password: "asd123ASD."
							}
						)
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(StudentErrors.FORMAT_SURNAME);
			});
	});

	it('Student - Register - Bad password', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					mutation {
						student_register(
							input: {
								email: "yoquese@gmail.com"
								name: "Albert"
								surname: "Maroto"
								password: "12312"
							}
						)
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(StudentErrors.FORMAT_PASSWORD);
			});
	});

	it('Student - Register - Email already registered', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					mutation {
						student_register(
							input: {
								email: "yoquese@gmail.com"
								name: "Albert"
								surname: "hvdev"
								password: "asd123ASD."
							}
						)
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(StudentErrors.EMAIL_IN_USE);
			});
	});

	it('Student - Activate Account', async () => {
		const tokenModel: Model<TokenDocument> = model(
			'StudentToken',
			StudentTokenSchema
		);
		const { token } = await tokenModel.findOne().exec();

		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
				mutation {
					student_activate_account(token: "${token}")
				}`,
			})
			.expect(200)
			.expect(async ({ body }) => {
				expect(body.data.student_activate_account).toBe(true);
				expect(await tokenModel.findOne().exec()).toBeNull();
			});
	});

	it('Student - Activate Account - Bad token', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					mutation {
						student_activate_account(token: "asdqwqweaz2d")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(CommonErrors.TOKEN_NOT_FOUND);
			});
	});

	it('Student - Create Forgot Password', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					mutation {
						student_create_forgot_password_token(email: "yoquese@gmail.com")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.data.student_create_forgot_password_token).toBe(true);
			});
	});

	it('Student - Create Forgot Password - Bad email', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					mutation {
						student_create_forgot_password_token(email: "asd123@gmail.com")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(StudentErrors.EMAIL_NOT_FOUND);
			});
	});

	it('Student - Valid Forgot Password', async () => {
		const tokenModel: Model<TokenDocument> = model(
			'StudentToken',
			StudentTokenSchema
		);
		const { token } = await tokenModel.findOne().exec();

		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
				query {
					student_valid_forgot_password_token(token: "${token}")
				}`,
			})
			.expect(200)
			.expect(async ({ body }) => {
				expect(body.data.student_valid_forgot_password_token).toBe(true);
			});
	});

	it('Student - Valid Forgot Password - Bad token', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					query {
						student_valid_forgot_password_token(token: "dsag3t4wds43")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(CommonErrors.TOKEN_NOT_FOUND);
			});
	});

	it('Student - Change Forgot Password', async () => {
		const tokenModel: Model<TokenDocument> = model(
			'StudentToken',
			StudentTokenSchema
		);
		const { token } = await tokenModel.findOne().exec();

		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
				mutation {
					student_change_forgot_password(input:{token: "${token}", newPassword: "asd123ASD."})
				}`,
			})
			.expect(200)
			.expect(async ({ body }) => {
				expect(body.data.student_change_forgot_password).toBe(true);
				expect(await tokenModel.findOne().exec()).toBeNull();
			});
	});

	it('Student - Change Forgot Password - Bad token', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					mutation {
						student_change_forgot_password(
							input: {
								token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
								newPassword: "asd123ASD."
							}
						)
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(CommonErrors.TOKEN_NOT_FOUND);
			});
	});

	it('Student - Login', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					query {
						student_login(
							input: { email: "yoquese@gmail.com", password: "asd123ASD." }
						) {
							token
						}
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(typeof body.data.student_login.token).toBe('string');
				authToken = body.data.student_login.token;
			});
	});

	it('Student - Login - Bad email', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					query {
						student_login(
							input: { email: "Ilya2@gmail.com", password: "asd123ASD." }
						) {
							token
						}
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toEqual(StudentErrors.INVALID_LOGIN);
			});
	});

	it('Student - Login - Bad password', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.send({
				query: gql`
					query {
						student_login(
							input: { email: "Ilya@gmail.com", password: "asd123ASD" }
						) {
							token
						}
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(StudentErrors.INVALID_LOGIN);
			});
	});

	it('Student - Profile', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					query {
						student_profile {
							email
							name
						}
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(typeof body.data.student_profile).toBe('object');
				expect(body.data.student_profile.name).toBe('Albert');
			});
	});

	it('Student - Profile - Bad token', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer 3fr5w25f4str43`)
			.send({
				query: gql`
					query {
						student_profile {
							email
							name
						}
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(CommonErrors.UNAUTHORIZED);
			});
	});

	it('Student - Change Password', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_change_password(
							input: { oldPassword: "asd123ASD.", newPassword: "asd123ASD.." }
						)
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.data.student_change_password).toBe(true);
			});
	});

	it('Student - Change Password - Bad format', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_change_password(
							input: { oldPassword: "asd123ASD.", newPassword: "asd" }
						)
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(StudentErrors.FORMAT_PASSWORD);
			});
	});

	it('Student - Change Password - Bad token', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer asd1234ass`)
			.send({
				query: gql`
					mutation {
						student_change_password(
							input: { oldPassword: "asd123ASD.", newPassword: "asd123ASD." }
						)
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(CommonErrors.UNAUTHORIZED);
			});
	});

	it('Student - Change Password - Bad old password', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_change_password(
							input: { oldPassword: "asd13ASD.", newPassword: "asd123ASD." }
						)
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(StudentErrors.FORMAT_OLD_PASSWORD);
			});
	});

	it('Student - Change Email', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_change_email(newEmail: "yoquese@gmail.com")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.data.student_change_email).toBe('yoquese@gmail.com');
			});
	});

	it('Student - Change Email - Bad format', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_change_email(newEmail: "asd")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(StudentErrors.FORMAT_EMAIL);
			});
	});

	it('Student - Change Email - Bad token', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer asd1234ass`)
			.send({
				query: gql`
					mutation {
						student_change_email(newEmail: "yoquese@gmail.com")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(CommonErrors.UNAUTHORIZED);
			});
	});

	it('Student - Change Username', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_change_username(newUsername: "Albert87")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.data.student_change_username).toBe('Albert87');
			});
	});

	it('Student - Change Username - Bar Username', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_change_username(newUsername: "Albert._Master")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(StudentErrors.FORMAT_USERNAME);
			});
	});

	it('Student - Change Username - Bad token', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer asd1234ass`)
			.send({
				query: gql`
					mutation {
						student_change_username(newUsername: "Albert87")
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(CommonErrors.UNAUTHORIZED);
			});
	});

	it('Student - Change Profile', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_modify_profile(
							input: {
								name: "Reiva"
								surname: "Outlaw"
								gender: "${Gender.MALE}"
								birthDate: "1990/07/15"
								bio: "Te cuento mi vida en verso"
							}
						){
							name
							surname
							gender
							birthDate
							bio
						}
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.data.student_modify_profile.name).toBe('Reiva');
				expect(body.data.student_modify_profile.surname).toBe('Outlaw');
				expect(body.data.student_modify_profile.gender).toBe(Gender.MALE);
				expect(body.data.student_modify_profile.birthDate).toBe(
					new Date('1990/07/15').toUTCString()
				);
				expect(body.data.student_modify_profile.bio).toBe(
					'Te cuento mi vida en verso'
				);
			});
	});

	it('Student - Change Profile - Bad token', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer asd1234ass`)
			.send({
				query: gql`
					mutation {
						student_modify_profile(
							input: {
								name: "Reiva"
								surname: "Outlaw"
								gender: "${Gender.MALE}"
								birthDate: "01/01/1990"
								bio: "Te cuento mi vida en verso"
							}
						){
							name
							surname
							gender
							birthDate
							bio
						}
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(CommonErrors.UNAUTHORIZED);
			});
	});

	it('Student - Change Profile - Bad BirthDate', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_modify_profile(
							input: {
								name: "Reiva"
								surname: "Outlaw"
								gender: "${Gender.MALE}"
								birthDate: "31/02/2020"
								bio: "Te cuento mi vida en verso"
							}
						){
							name
							surname
							gender
							birthDate
							bio
						}
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(StudentErrors.FORMAT_BIRTHDATE);
			});
	});

	it('Student - Change Profile - Bad Name', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_modify_profile(
							input: {
								name: "Reiva_Crack"
								surname: "Outlaw"
								gender: "${Gender.MALE}"
								birthDate: "07/15/1990"
								bio: "Te cuento mi vida en verso"
							}
						){
							name
							surname
							gender
							birthDate
							bio
						}
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(StudentErrors.FORMAT_NAME);
			});
	});

	it('Student - Change Profile - Bad Surname', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_modify_profile(
							input: {
								name: "Reiva"
								surname: "Outlaw7"
								gender: "${Gender.MALE}"
								birthDate: "07/15/1990"
								bio: "Te cuento mi vida en verso"
							}
						){
							name
							surname
							gender
							birthDate
							bio
						}
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message).toBe(StudentErrors.FORMAT_SURNAME);
			});
	});

	it('Student - Change Profile - Bad Name and BirthDate', () => {
		return request(app.getHttpServer())
			.post('/graphql')
			.set('Authorization', `Bearer ${authToken}`)
			.send({
				query: gql`
					mutation {
						student_modify_profile(
							input: {
								name: "Reiva_Crack"
								surname: "Outlaw"
								gender: "${Gender.MALE}"
								birthDate: "15/07/1990"
								bio: "Te cuento mi vida en verso"
							}
						){
							name
							surname
							gender
							birthDate
							bio
						}
					}
				`,
			})
			.expect(200)
			.expect(({ body }) => {
				expect(body.errors[0].message.split('. ')).toContain(
					StudentErrors.FORMAT_NAME
				);
				expect(body.errors[0].message.split('. ')).toContain(
					StudentErrors.FORMAT_BIRTHDATE
				);
			});
	});

	it('Student - Upload photo', () => {
		return request(app.getHttpServer())
			.post('/student/upload')
			.set('Authorization', `Bearer ${authToken}`)
			.attach('photo', join(process.cwd(), './photo.jpg'))
			.expect(201)
			.expect(({ body }) => {
				expect(typeof body.url).toBe('string');
			});
	});

	it('Student - Upload photo- Bad token', () => {
		return request(app.getHttpServer())
			.post('/student/upload')
			.set('Authorization', `Bearer ahipuesno`)
			.attach('photo', join(process.cwd(), './photo.jpg'))
			.expect(401);
	});

	it('Student - Upload photo - Bad extension', () => {
		return request(app.getHttpServer())
			.post('/student/upload')
			.set('Authorization', `Bearer ${authToken}`)
			.attach('photo', join(process.cwd(), './.env.example'))
			.expect(400);
	});

	afterAll(async () => {
		await app.close();
		await disconnect();
		await replSet.stop();
	});
});
